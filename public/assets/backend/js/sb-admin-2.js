$(function() {
    $('#side-menu').metisMenu();

});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});
function checkAll(ele, what) {
    var selectWhat;
    if (what == "_all") {
        selectWhat = "[id*='" + what + "'],[id*='_view'],[id*='_edit'],[id*='_add'],[id*='_delete']";
    } else {
        selectWhat = "[id*='" + what + "']";
    }
    var rem = document.querySelectorAll(selectWhat);
    if (ele.checked) {
        for (var i = 0; i < rem.length; i++) {
            if (rem[i].type == 'checkbox') {
                rem[i].checked = true;
            }
        }
    } else {
        for (var i = 0; i < rem.length; i++) {
            if (rem[i].type == 'checkbox') {
                rem[i].checked = false;
            }
        }
    }
}

//$(function () {
//    if($(".btn.btn-success").is(":submit")) {
//        $(".btn.btn-success").addClass("disabled");
//    }
//    $(".form-control").bind("change keyup keydown", function (el) {
//        var $this = $(el.target);
//        var _old, _new,_tag = $this.prop("tagName").toLowerCase();
//        if ("input" === _tag) {
//            _old = $this.attr("value");
//            _new = $this.prop("value");
//            if(_old !== _new) {
//                if($(".btn.btn-success").is(":submit")){    
//                    $(".btn.btn-success").removeClass("disabled");
//                }
//            }
//        }else if("textarea" === _tag){
//            
//        }
//        
//        if (_old === _new) {
//            $(".btn.btn-success").addClass("disabled");
//        }
//    });
//});