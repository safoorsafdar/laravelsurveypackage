<?php

namespace Vteam\Survey;

use Illuminate\Support\ServiceProvider;

class SurveyServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot() {
        $this->package('vteam/survey');
        include __DIR__ . '/../../routes.php'; //including routes
        include __DIR__ . '/../../filters.php'; //including filters
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app['survey'] = $this->app->share(function($app) {
            return new Survey;
        });

        $this->app['SurveyCommand'] = $this->app->share(function($app) {
            return new SurveyCommand();
        });

        $this->commands('SurveyCommand');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return array("survey");
    }

}
