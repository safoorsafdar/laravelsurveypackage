<?php
namespace Vteam\Survey\Facades;
use Illuminate\Support\Facades\Facade;
/**
 * Description of Acl
 *
 * @author safoor
 */
class Survey extends Facade {
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'survey'; }
}
