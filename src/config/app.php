<?php

return array(
    'providers' => array(
        'Cartalyst\Sentry\SentryServiceProvider',
    ),
    'aliases' => array(
        'Sentry' => 'Cartalyst\Sentry\Facades\Laravel\Sentry',
    ),
);
