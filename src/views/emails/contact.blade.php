@extends('survey::emails/layouts/default')
@section('content')
<table>
    <tbody>
        <tr>
            <td>Name:</td>
            <td>{{$name}}</td>
        </tr>
        <tr>
            <td>Phone Number:</td>
            <td>{{$phone}}</td>
        </tr>
        <tr>
            <td>Email Address:</td>
            <td>{{$email}}</td>
        </tr>
        <tr>
            <td>Message:</td>
            <td>{{$message_body}}</td>
        </tr>
    </tbody>
</table>
@stop
