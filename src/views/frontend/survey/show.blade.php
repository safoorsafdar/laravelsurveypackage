@extends('survey::frontend/layout/master')
@section('title')
{{$survey_detail->name}} - 
@parent
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{$survey_detail->name}}</h1>
        <p class="lead">{{$survey_detail->detail}}</p>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        {{-- HTML::ul($errors->all()) --}}
        <!--        @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <h4>Error</h4>
                    {{ $message }}
                </div>
                @endif-->
        {{ $errors->first('ans', '<div class="alert alert-danger alert-block"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>Error</h4>:message</div>') }}
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <h3>Available Question <span class="label label-default">1/2</span></h3>
    </div>
</div>

<div id="rootwizard">
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container">
                <ul>
                    {{--"<pre>"--}}
                    {{--@print_r($questions)--}}
                    {{--"</pre>"--}}
                    <?php $i = 1; ?>
                    @foreach($questions as $question)
                    <li><a href="#tab{{$question->id}}" data-toggle="tab">{{$i}}</a></li>
                    <?php $i = $i + 1; ?>
                    @endforeach 
                </ul>
            </div>
        </div>
    </div>
    {{ Form::open(array('class'=>'form-horizontal','role'=>'form',"id"=>"survey_form","data-parsley-validate"=>"data-parsley-validate")) }}
    <div class="tab-content">
        <?php $i=0;?>
        @foreach($questions as $question)
        <div class="tab-pane" id="tab{{$question->id}}">
            <p>{{$question->name}}</p>
            <?php
            switch ($question->type) {
                case "radio":
                    foreach (Vteam\Survey\Model\Choice::where('question_id', '=', $question->id)->get() as $choice) {
                        echo '<label class="radio-inline">';
                        echo Form::radio('ans[' . $question->id . '][radio]', $choice->id,null,array("data-parsley-mincheck"=>"1","required"=>"required","data-parsley-group"=>"block_".$i."","data-parsley-errors-container"=>"#question_error"));
                        echo "&nbsp" . $choice->detail;
                        echo '</label>';
                    }
                    break;
                case "checkbox":
                    foreach (Vteam\Survey\Model\Choice::where('question_id', '=', $question->id)->get() as $choice) {
                        echo '<label class="checkbox-inline">';
                        echo Form::checkbox('ans[' . $question->id . '][box][]', $choice->id,null,array("data-parsley-mincheck"=>"2","required"=>"required","data-parsley-group"=>"block_".$i."","data-parsley-errors-container"=>"#question_error"));
                        echo "&nbsp" . $choice->detail;
                        echo '</label>';
                    }
                    break;
                case "textarea":
                    $choice_opt = Vteam\Survey\Model\Choice::where('question_id', '=', $question->id)->get();
                    if (sizeof($choice_opt) > 0) {
                        foreach ($choice_opt as $choice) {
                            echo Form::textarea('ans[' . $question->id . '][text]',NULL,array('class'=>'form-control', 'placeholder'=>'Please enter your phone number.',"required"=>"required","data-parsley-group"=>"block_".$i."","data-parsley-errors-container"=>"#question_error"));
                            break;
                        }
                    } else {
                        echo Form::textarea('ans[' . $question->id . '][text]',NULL,array('class'=>'form-control', 'placeholder'=>'Please enter your phone number.',"required"=>"required","data-parsley-group"=>"block_".$i."","data-parsley-errors-container"=>"#question_error"));
                    }
                    break;
                default :
                    echo "No Question Type Available.";
            }
            ?>
        </div>
        <?php $i++;?>
        @endforeach
        <span id="question_error"></span>
        <ul class="pager wizard">
            <li class="previous first"><a href="javascript:;">First</a></li>
            <li class="previous"><a href="javascript:;">Previous</a></li>
            <li class="next last"><a href="javascript:;">Last</a></li>
            <li class="next"><a href="javascript:;">Next</a></li>
            <li class="next finish" style="display:none;">{{ Form::submit('Complete', array('class'=>'btn btn-success'))}}</li>
        </ul>
    </div>
    {{ Form::close() }}
</div>
@stop