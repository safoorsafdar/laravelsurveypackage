@extends('survey::frontend/layout/master')
@section('title')
Account Sign up -
@parent
@stop
@section('content')
<div class="page-header">
    <h3>Sign up</h3>
</div>
<div class="row">
    <div class="col-lg-12">
        {{ Form::open(array('class'=>'form-horizontal','role'=>'form',"data-parsley-validate"=>"data-parsley-validate")) }}
        <!-- First Name -->
        <div class="form-group {{ $errors->first('first_name', ' has-error') }}">
            {{Form::label('first_name', 'First Name',array("class"=>"control-label col-sm-2"))}}
            <div class="col-sm-5">
                {{ Form::text('first_name', Input::old('first_name'), array('class'=>'form-control', 'placeholder'=>'Please enter your first name.','autofocus'=>'autofocus',"data-parsley-trigger"=>"change","required"=>"required")) }}
                {{$errors->first('first_name', '<span class="help-block">:message</span>')}}
            </div>
        </div>
        <!-- Last Name -->
        <div class="form-group {{ $errors->first('last_name', ' has-error') }}">
            {{Form::label('last_name', 'Last Name',array("class"=>"control-label col-sm-2"))}}
            <div class="col-sm-5">
                {{ Form::text('last_name', Input::old('last_name'), array('class'=>'form-control', 'placeholder'=>'Please enter your last name.',"data-parsley-trigger"=>"change","required"=>"required")) }}
                {{$errors->first('last_name', '<span class="help-block">:message</span>')}}
            </div>
        </div>
        <!-- Email -->
        <div class="form-group {{ $errors->first('email', ' has-error') }}">
            {{Form::label('email', 'Email',array("class"=>"control-label col-sm-2"))}}
            <div class="col-sm-5">
                {{ Form::text('email', Input::old('email'), array('class'=>'form-control', 'placeholder'=>'Please enter your email.',"data-parsley-trigger"=>"change","required"=>"required","data-parsley-type"=>"email")) }}
                {{$errors->first('email', '<span class="help-block">:message</span>')}}
            </div>
        </div>
        <!-- Email Confirm -->
        <div class="form-group {{ $errors->first('email_confirm', ' has-error') }}">
            {{Form::label('email_confirm', 'Confirm Email',array("class"=>"control-label col-sm-2"))}}
            <div class="col-sm-5">
                {{ Form::text('email_confirm', Input::old('email_confirm'), array('class'=>'form-control', 'placeholder'=>'Please confirm your email.',"data-parsley-trigger"=>"change","required"=>"required","data-parsley-type"=>"email","data-parsley-equalto"=>"#email")) }}
                {{$errors->first('email_confirm', '<span class="help-block">:message</span>')}}
            </div>
        </div>
        <!-- Password -->
        <div class="form-group {{ $errors->first('password', ' has-error') }}">
            {{Form::label('password', 'Password',array("class"=>"control-label col-sm-2"))}}
            <div class="col-sm-5">
                {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Please enter your password',"data-parsley-trigger"=>"change","required"=>"required")) }}
                {{$errors->first('password', '<span class="help-block">:message</span>')}}
            </div>
        </div>

        <!-- Password Confirm -->
        <div class="form-group {{ $errors->first('password_confirm', ' has-error') }}">
            {{Form::label('password_confirm', 'Confirm Password',array("class"=>"control-label col-sm-2"))}}
            <div class="col-sm-5">
                {{ Form::password('password_confirm', array('class'=>'form-control', 'placeholder'=>'Please confirm your password',"data-parsley-trigger"=>"change","required"=>"required","data-parsley-equalto"=>"#password")) }}
                {{$errors->first('password_confirm', '<span class="help-block">:message</span>')}}
            </div>
        </div>
        <hr>
        <!-- Form actions -->
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <a class="btn" href="{{ route('home') }}">Cancel</a>
                {{ Form::submit('Sign up', array('class'=>'btn btn-success'))}}
                <a href="{{ route('user/login') }}" class="btn btn-link">Login</a>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop