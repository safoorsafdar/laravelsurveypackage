@extends('survey::frontend/layout/master')
@section('title')
Login - 
@parent
@stop
@section('content')

<div class="page-header">
    <h3>Sign in into your Account</h3>
</div>
<div class="row">
    <div class="col-lg-12">
        {{ Form::open(array('class'=>'form-horizontal','role'=>'form',"data-parsley-validate"=>"data-parsley-validate")) }}
        <div class="form-group {{ $errors->first('email', ' has-error') }}">
            {{Form::label('email', 'Email',array("class"=>"control-label col-sm-2"))}}
            <div class="col-sm-5">
                {{ Form::text('email', Input::old('email'), array('class'=>'form-control','autofocus'=>'autofocus','placeholder'=>'Please enter your email.',"data-parsley-trigger"=>"change","required"=>"required","data-parsley-type"=>"email")) }}
                {{$errors->first('email', '<span class="help-block">:message</span>')}}
            </div>
        </div>

        <!-- Password -->
        <div class="form-group {{ $errors->first('password', ' has-error') }}">
            {{Form::label('password', 'Password',array("class"=>"control-label col-sm-2"))}}
            <div class="col-sm-5">
                {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Provide enter your password')) }}
                {{$errors->first('password', '<span class="help-block">:message</span>')}}
            </div>
        </div>

        <!-- Remember me -->
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember-me" id="remember-me" value="1" /> Remember me
                    </label>
                </div>
            </div>
        </div>
        <hr>
        <!-- Form actions -->
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <a class="btn" href="{{ route('home') }}">Cancel</a>
                {{ Form::submit('Sign in', array('class'=>'btn btn-success'))}}
                <a href="{{ route('user/forgot-password') }}" class="btn btn-link">I forgot my password</a>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop