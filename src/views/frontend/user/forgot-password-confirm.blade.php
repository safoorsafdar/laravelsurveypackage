@extends('survey::frontend/layout/master')

{{-- Page title --}}
@section('title')
Forgot Password ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="page-header"><h3>Forgot Password</h3></div>
{{ Form::open(array('class'=>'form-horizontal','role'=>'form',"data-parsley-validate"=>"data-parsley-validate")) }}
<!-- Password -->
<div class="form-group {{ $errors->first('password', ' has-error') }}">
    {{Form::label('password', 'New Password',array("class"=>"control-label col-sm-2"))}}
    <div class="col-sm-5">
        {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Provide enter your password',"data-parsley-trigger"=>"change","required"=>"required")) }}
        {{$errors->first('password', '<span class="help-block">:message</span>')}}
    </div>
</div>

<!-- Password Confirm -->
<div class="form-group {{ $errors->first('password_confirm', ' has-error') }}">
    {{Form::label('password_confirm', 'Confirm Password',array("class"=>"control-label col-sm-2"))}}
    <div class="col-sm-5">
        {{ Form::password('password_confirm', array('class'=>'form-control', 'placeholder'=>'Please confirm your password',"data-parsley-trigger"=>"change","required"=>"required","data-parsley-equalto"=>"#password")) }}
        {{$errors->first('password_confirm', '<span class="help-block">:message</span>')}}
    </div>
</div>


<!-- Form actions -->
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <a class="btn" href="{{ route('home') }}">Cancel</a>
        {{ Form::submit('Submit', array('class'=>'btn btn-success'))}}
    </div>
</div>
</form>
@stop
