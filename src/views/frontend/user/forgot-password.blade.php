@extends('survey::frontend/layout/master')

{{-- Page title --}}
@section('title')
Forgot Password ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="page-header">
    <h3>Forgot Password</h3>
</div>
{{ Form::open(array('class'=>'form-horizontal','role'=>'form',"data-parsley-validate"=>"data-parsley-validate")) }}
<div class="form-group {{ $errors->first('email', ' has-error') }}">
    {{Form::label('email', 'Email',array("class"=>"control-label col-sm-2"))}}
    <div class="col-sm-5">
        {{ Form::text('email', Input::old('email'), array('class'=>'form-control','autofocus'=>'autofocus','placeholder'=>'Please enter your email.',"data-parsley-trigger"=>"change","required"=>"required","data-parsley-type"=>"email")) }}
        {{$errors->first('email', '<span class="help-block">:message</span>')}}
    </div>
</div>
<!-- Form actions -->
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <a class="btn" href="{{ route('home') }}">Cancel</a>
        {{ Form::submit('Submit', array('class'=>'btn btn-success'))}}
    </div>
</div>
{{ Form::close() }}
@stop
