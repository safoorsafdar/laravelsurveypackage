
<!-- jQuery -->
<script src="{{asset('packages/vteam/survey/libs/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{asset('packages/vteam/survey/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('packages/vteam/survey/libs/wizard/jquery.bootstrap.wizard.min.js')}}"></script>
<script src="{{asset('packages/vteam/survey/libs/parsley/js/parsley.min.js')}}"></script>
<footer>
    <div class="container">
        <hr>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright © Survey {{date("Y")}}</p>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript">
$(document).ready(function () {
    $('#rootwizard').bootstrapWizard(
            {
                onTabShow: function (tab, navigation, index) {
                    var $current = index + 1;
                    var $total = navigation.find('li').length;
                    $(".label.label-default").html($current + "/" + $total);

                    // If it's the last tab then hide the last button and show the finish instead
                    if ($current >= $total) {
                        $('#rootwizard').find('.pager .next').hide();
                        $('#rootwizard').find('.pager .finish').show();
                        $('#rootwizard').find('.pager .finish').removeClass('disabled');
                    } else {
                        $('#rootwizard').find('.pager .next').show();
                        $('#rootwizard').find('.pager .finish').hide();
                    }
                }, onNext: function (tab, navigation, index) {

                    if (false === $('#survey_form').parsley().validate('block_' + (index - 1))) {
                        return false;
                    }
                    ;
                }
            }
    );
    $('#rootwizard .finish').click(function () {
//        alert('Finished!, Starting over!');
        if (false === $('#survey_form').parsley().validate('block_' + ($('#rootwizard').find("a[href*='tab']").length))) {
            return false;
        }
        
    });
});
</script>