<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{route("home")}}">SurveyApp</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <!--<li><a href="#">Services</a></li>-->
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{route("about")}}">About</a></li>
                <li><a href="{{route("contact")}}">Contact</a></li>
                <!-- Show only when user loggedin -->
                @if(!Sentry::check()) 
                    <li><a href="{{route("user/login")}}">Login</a></li>
                    <li><a href="{{route("user/signup")}}">Signup</a></li>
                @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> Welcome [{{ucfirst(Sentry::getUser()->first_name)}}] <i class="fa fa-caret-down"></i></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i>&nbsp;Profile</a></li>
                        <li><a href="#"><i class="fa fa-gears"></i>&nbsp;Setting</a></li>
                        <li class="divider"></li>
                        <li><a href="{{route("user/logout")}}"><i class="fa fa-sign-out fa-fw"></i>&nbsp;Logout</a></li>
                    </ul>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>