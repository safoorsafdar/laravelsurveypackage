@extends('survey::frontend/layout/master')
@section('title')
Contact
@parent
@stop
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Contact</h1>
        </div>
    </div>
    <div class="row">
        <!-- Map Column -->
        <div class="col-md-8">
            <!-- Embedded Google Map -->
            <iframe width="100%" height="400px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?hl=en&amp;ie=UTF8&amp;ll=37.0625,-95.677068&amp;spn=56.506174,79.013672&amp;t=m&amp;z=4&amp;output=embed"></iframe>
        </div>
        <!-- Contact Details Column -->
        <div class="col-md-4">
            <h3>Contact Details</h3>
            <p>
                3481 Melrose Place<br>Beverly Hills, CA 90210<br>
            </p>
            <p><i class="fa fa-phone"></i> 
                <abbr title="Phone">P</abbr>: (123) 456-7890</p>
            <p><i class="fa fa-envelope-o"></i> 
                <abbr title="Email">E</abbr>: <a href="mailto:name@example.com">name@example.com</a>
            </p>
            <p><i class="fa fa-clock-o"></i> 
                <abbr title="Hours">H</abbr>: Monday - Friday: 9:00 AM to 5:00 PM</p>
            <ul class="list-unstyled list-inline list-social-icons">
                <li><a href="#"><i class="fa fa-facebook-square fa-2x"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin-square fa-2x"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter-square fa-2x"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus-square fa-2x"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <h3>Send us a Message</h3>
            {{ Form::open(array('role'=>'form',"data-parsley-validate"=>"data-parsley-validate")) }}
            <div class="form-group {{ $errors->first('name', ' has-error') }}">
                {{Form::label('name', 'Name:')}}
                {{Form::text('name',NULL, array('class'=>'form-control', 'placeholder'=>'Please enter your name.','autofocus'=>'autofocus',"data-parsley-trigger"=>"change","required"=>"required"))}}
                {{$errors->first('name', '<span class="help-block">:message</span>')}}
            </div>
            <div class="form-group {{ $errors->first('phone', ' has-error') }}">
                {{Form::label('phone', 'Phone Number:')}}
                {{Form::text('phone',NULL, array('class'=>'form-control', 'placeholder'=>'Please enter your phone number.',"data-parsley-trigger"=>"change","required"=>"required",))}}
                {{$errors->first('phone', '<span class="help-block">:message</span>')}}
            </div>
            <div class="form-group {{ $errors->first('email', ' has-error') }}">
                {{Form::label('email', 'Email Address:')}}
                {{Form::text('email',NULL, array('class'=>'form-control', 'placeholder'=>'Please enter your email address.',"data-parsley-trigger"=>"change","required"=>"required","data-parsley-type"=>"email"))}}
                {{$errors->first('email', '<span class="help-block">:message</span>')}}
            </div>
            <div class="form-group {{ $errors->first('message', ' has-error') }}">
                {{Form::label('message', 'Message:')}}
                {{Form::textarea('message',NULL, array('class'=>'form-control', 'placeholder'=>'Please enter your message.',"maxlength"=>"999","style"=>"resize:none;","data-parsley-trigger"=>"change","required"=>"required",))}}
                {{$errors->first('message', '<span class="help-block">:message</span>')}}
            </div>
            {{ Form::submit('Send Message', array('class'=>'btn btn-primary'))}}
            {{ Form::close() }}
        </div>

    </div>



</div>
@stop