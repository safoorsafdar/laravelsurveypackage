<!DOCTYPE html>
<html lang="en">
    <head>
        @include('survey::frontend/inc/head')
    </head>
    <body>
        @include('survey::frontend/inc/nav')
        <div class="container">
            @include('survey::notifications')
            @yield('content')
        </div>
        @include('survey::frontend/inc/foot')
    </body>
</html>