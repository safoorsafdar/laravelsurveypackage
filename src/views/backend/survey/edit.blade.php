@extends('survey::backend/layouts/dashboard')
@section('title')
Update a Survey ::
@parent
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h3>
                Update Survey
                <div class="pull-right">
                    <a href="{{ route('survey') }}" class="btn-sm btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
            </h3>
        </div>
        {{ Form::open(array('class'=>'form-horizontal','role'=>'form',"data-parsley-validate"=>"data-parsley-validate")) }}
        {{-- Form::model($detail, array('route' => array('update/survey', $detail->id), 'method' => 'PUT')) --}}
        <fieldset>
            <legend>General Information</legend>
            <div class="form-group">
                
                {{Form::label('name', 'Name',array("class"=>"control-label col-sm-2"))}}
                <div class="col-sm-5">
                    {{ Form::text('name', Input::old('name', $detail->name), array('class'=>'form-control', 'placeholder'=>'Name','autofocus'=>'autofocus',"data-parsley-trigger"=>"change","required"=>"required")) }}
                    {{$errors->first('name', '<span class="help-block">:message</span>')}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('detail', 'Detail',array("class"=>"control-label col-sm-2"))}}
                <div class="col-sm-5">
                    {{ Form::textarea('detail', Input::old('detail', $detail->detail), array('class'=>'form-control', 'placeholder'=>'Detail',"data-parsley-trigger"=>"change","required"=>"required")) }}
                    {{$errors->first('detail', '<span class="help-block">:message</span>')}}
                </div>
            </div>
        </fieldset>
        <hr>
        <div class="form-group">
            <div class="col-md-12">
                <a class="btn btn-link" href="{{ route('survey') }}">Cancel</a>
                <button type="reset" class="btn">Reset</button>
                {{ Form::submit('Update Survey', array('class'=>'btn btn-success'))}}
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop
