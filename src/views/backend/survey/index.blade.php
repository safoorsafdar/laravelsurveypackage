@extends('survey::backend/layouts/dashboard')
@section('title')
Survey Management ::
@parent
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h3>
                Survey's Management
                <div class="pull-right">
                    <a href="{{route('create/survey')}}" class="btn btn-small btn-info"><i class="fa fa-plus"></i> Create</a>
                </div>
            </h3>
        </div>
        {{$surveys->links()}}

        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th class="span1">ID</th>
                    <th class="span2">Name</th>
                    <th class="span2">Created At</th>
                    <th class="span2">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($surveys as $survey)
                <tr>
                    <td>{{ $survey->id }}</td>
                    <td>{{ $survey->name }}</td>
                    <td>{{ $survey->created_at->diffForHumans() }}</td>
                    <td>
                        <a href="{{ route('update/survey', $survey->id) }}" class="btn btn-sm btn-default">@lang('survey::button.edit')</a>
                        @if ( ! is_null($survey->deleted_at))
                            <a href="{{ route('restore/survey', $survey->id) }}" class="btn btn-sm btn-warning">@lang('survey::button.restore')</a>
                        @else
                            <a href="{{ route('delete/survey', $survey->id) }}" class="btn btn-sm btn-danger">@lang('survey::button.delete')</a>
                            <!--<span class="btn btn-sm btn-danger disabled">@lang('button.delete')</span>-->
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        {{ $surveys->links() }}    
    </div>
</div>
@stop
