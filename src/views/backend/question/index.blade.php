@extends('survey::backend/layouts/dashboard')
@section('title')
Question Management ::
@parent
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h3>
                Question's Management
                <div class="pull-right">
                    <a href="{{route('create/question')}}" class="btn btn-small btn-info"><i class="fa fa-plus"></i> Create</a>
                </div>
            </h3>
        </div>
        {{$questions->links()}}

        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th class="span1">ID</th>
                    <th class="span2">Name</th>
                    <th class="span2">Exam</th>
                    <th class="span2">Created At</th>
                    <th class="span2">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($questions as $question)
                <tr>
                    <td>{{ $question->id }}</td>
                    <td>{{ $question->name }}</td>
                    <td>{{-- $question->surveym->name  --}}{{Vteam\Survey\Model\SurveyM::find($question->exam_id)->first()->name}}</td>
                    <td>{{ $question->created_at->diffForHumans() }}</td>
                    <td>
                        <a href="{{ route('update/question', $question->id) }}" class="btn btn-sm btn-default">@lang('survey::button.edit')</a>
                        @if ( ! is_null($question->deleted_at))
                            <a href="{{ route('restore/question', $question->id) }}" class="btn btn-sm btn-warning">@lang('survey::button.restore')</a>
                        @else
                            <a href="{{ route('delete/question', $question->id) }}" class="btn btn-sm btn-danger">@lang('survey::button.delete')</a>
                            <!--<span class="btn btn-sm btn-danger disabled">@lang('button.delete')</span>-->
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        {{ $questions->links() }}    
    </div>
</div>
@stop
