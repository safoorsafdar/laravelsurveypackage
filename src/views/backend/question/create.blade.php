@extends('survey::backend/layouts/dashboard')
@section('title')
Create a Question ::
@parent
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h3>
                Create a New Question
                <div class="pull-right">
                    <a href="{{ route('question') }}" class="btn-sm btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
            </h3>
        </div>
        {{ Form::open(array('class'=>'form-horizontal','role'=>'form',"data-parsley-validate"=>"data-parsley-validate")) }}
        <fieldset>
            <legend>General Information</legend>
            <div class="form-group">
                {{Form::label('name', 'Name',array("class"=>"control-label col-sm-2"))}}
                <div class="col-sm-5">
                    {{ Form::text('name', NULL, array('class'=>'form-control', 'placeholder'=>'Name','autofocus'=>'autofocus',"data-parsley-trigger"=>"change","required"=>"required")) }}
                    {{$errors->first('name', '<span class="help-block">:message</span>')}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('type', 'Type',array("class"=>"control-label col-sm-2"))}}
                <div class="col-sm-5">
                    {{ Form::select('type', array('' => '-- Select --','radio' => 'Radio', 'checkbox' => 'Checkbox', 'textarea' => 'textarea'),null,array("data-parsley-trigger"=>"change","required"=>"required")); }}
                    {{$errors->first('type', '<span class="help-block">:message</span>')}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('surveyID', 'Survey',array("class"=>"control-label col-sm-2"))}}
                <div class="col-sm-5">
                    <select name="surveyID" id="surveyID" required="required">
                        <option value="">-- Select --</option>
                        @foreach ($surveyList as $group)
                            <option value="{{ $group->id }}">{{ $group->name }}</option>
			@endforeach
                    </select>
                    {{$errors->first('surveyID', '<span class="help-block">:message</span>')}}
                </div>
            </div>
        </fieldset>
        <hr>
        <div class="form-group">
            <div class="col-md-12">
                <a class="btn btn-link" href="{{ route('question') }}">Cancel</a>
                <button type="reset" class="btn">Reset</button>
                {{ Form::submit('Create Question', array('class'=>'btn btn-success'))}}
            </div>
        </div>
        {{ Form::close() }}


    </div>
</div>
@stop
