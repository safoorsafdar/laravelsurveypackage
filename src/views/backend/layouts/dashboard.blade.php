
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>@section('title')
            Administration
            @show
        </title>

        <!-- Bootstrap Core CSS -->
        <link href="{{asset('packages/vteam/survey/libs/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="{{asset('packages/vteam/survey/libs/metisMenu/dist/metisMenu.min.css')}}" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="{{ asset('packages/vteam/survey/assets/backend/css/timeline.css')}}" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="{{ asset('packages/vteam/survey/assets/backend/css/sb-admin-2.css')}}" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="{{asset('packages/vteam/survey/libs/morrisjs/morris.css')}}" rel="stylesheet" type="text/css">
        <!-- Custom Fonts -->
        <link href="{{asset('packages/vteam/survey/libs/fontawesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('packages/vteam/survey/libs/parsley/css/style.css')}}" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            @if (\Sentry::check()) 
            @include('survey::backend/layouts/nav')
            @endif
            <div id="page-wrapper">
                @include('survey::notifications')
                @yield('content') 
            </div>
            <footer class="footer">
                <p class="text-muted">Copyright © SurveyApp {{date("Y")}}</p>
            </footer>
        </div>
        <!-- /#wrapper -->
        <!-- jQuery -->
        <script src="{{asset('packages/vteam/survey/libs/jquery/dist/jquery.min.js')}}"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="{{asset('packages/vteam/survey/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <!-- Metis Menu Plugin JavaScript -->
        <script src="{{asset('packages/vteam/survey/libs/metisMenu/dist/metisMenu.min.js')}}"></script>
        @if(Request::path()==="admin")
        <!-- Morris Charts JavaScript -->
        <script src="{{asset('packages/vteam/survey/libs/raphael/raphael-min.js')}}"></script>
        <script src="{{asset('packages/vteam/survey/libs/morrisjs/morris.min.js')}}"></script>
        <script src="{{ asset('packages/vteam/survey/assets/backend/js/morris-data.js') }}"></script>
        @endif
        <!-- Custom Theme JavaScript -->
        <script src="{{ asset('packages/vteam/survey/assets/backend/js/sb-admin-2.js') }}"></script>
        <script src="{{asset('packages/vteam/survey/libs/parsley/js/parsley.min.js')}}"></script>
    </body>
</html>
