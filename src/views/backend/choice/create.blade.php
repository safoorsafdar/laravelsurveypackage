@extends('survey::backend/layouts/dashboard')
@section('title')
Create a Choice ::
@parent
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h3>
                Create a New Choice
                <div class="pull-right">
                    <a href="{{ route('choice') }}" class="btn-sm btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
            </h3>
        </div>
        {{ Form::open(array('class'=>'form-horizontal','role'=>'form',"data-parsley-validate"=>"data-parsley-validate")) }}
        <fieldset>
            <legend>General Information</legend>
            <div class="form-group">
                {{Form::label('name', 'Detail',array("class"=>"control-label col-sm-2"))}}
                <div class="col-sm-5">
                    {{ Form::text('name', NULL, array('class'=>'form-control', 'placeholder'=>'Name','autofocus'=>'autofocus',"data-parsley-trigger"=>"change","required"=>"required")) }}
                    {{$errors->first('name', '<span class="help-block">:message</span>')}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('questionID', 'Question',array("class"=>"control-label col-sm-2"))}}
                <div class="col-sm-5">
                    <select name="questionID" id="questionID" required="required">
                        <option value="">-- Select --</option>
                        @foreach ($questionList as $question)
                            <option value="{{ $question->id }}">{{ $question->name }}</option>
			@endforeach
                    </select>
                    {{$errors->first('questionID', '<span class="help-block">:message</span>')}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('isAnswer', 'Is Answer?',array("class"=>"control-label col-sm-2"))}}
                <div class="col-sm-5">
                    <select name="isAnswer" id="isAnswer" required="required">
                        <option value="no"{{ (Input::old('isAnswer', 0) === 'no' ? ' selected="selected"' : '') }}>@lang('survey::general.no')</option>
                        <option value="yes"{{ (Input::old('isAnswer', 0) === 'yes' ? ' selected="selected"' : '') }}>@lang('survey::general.yes')</option>
                    </select>
                    {{$errors->first('isAnswer', '<span class="help-block">:message</span>')}}
                </div>
            </div>
        </fieldset>
        <hr>
        <div class="form-group">
            <div class="col-md-12">
                <a class="btn btn-link" href="{{ route('choice') }}">Cancel</a>
                <button type="reset" class="btn">Reset</button>
                {{ Form::submit('Create Choice', array('class'=>'btn btn-success'))}}
            </div>
        </div>
        {{ Form::close() }}


    </div>
</div>
@stop
