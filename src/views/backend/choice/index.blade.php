@extends('survey::backend/layouts/dashboard')
@section('title')
Choice Management ::
@parent
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h3>
                Choices's Management
                <div class="pull-right">
                    <a href="{{route('create/choice')}}" class="btn btn-small btn-info"><i class="fa fa-plus"></i> Create</a>
                </div>
            </h3>
        </div>
        {{$choices->links()}}

        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th class="span1">ID</th>
                    <th class="span2">Detail</th>
                    <th class="span2">IsAnswer</th>
                    <th class="span2">Question</th>
                    <th class="span2">Created At</th>
                    <th class="span2">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($choices as $choice)
                <tr>
                    <td>{{ $choice->id }}</td>
                    <td>{{ $choice->detail }}</td>
                    <td>{{ ucfirst($choice->isAnswer) }}</td>
                    <td>{{ Vteam\Survey\Model\Question::find($choice->question_id)->name }} {{--$user->exam()->name--}}</td>
                    <td>{{ $choice->created_at->diffForHumans() }}</td>
                    <td>
                        <a href="{{ route('update/choice', $choice->id) }}" class="btn btn-sm btn-default">@lang('survey::button.edit')</a>
                        @if ( ! is_null($choice->deleted_at))
                            <a href="{{ route('restore/choice', $choice->id) }}" class="btn btn-sm btn-warning">@lang('survey::button.restore')</a>
                        @else
                            <a href="{{ route('delete/choice', $choice->id) }}" class="btn btn-sm btn-danger">@lang('survey::button.delete')</a>
                            <!--<span class="btn btn-sm btn-danger disabled">@lang('button.delete')</span>-->
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        {{ $choices->links() }}    
    </div>
</div>
@stop
