@extends('survey::backend/layouts/dashboard')
@section('title')
Change your Password
@parent
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 panel-body">
        <ul class="nav nav-tabs nav-justified">
            <li><a href="{{ route('profile') }}">Your Profile</a></li>
            <li class="active"><a href="{{route('change-password')}}">Change Password</a></li>
            <li><a href="{{route('change-email')}}">Change Email</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <h2 class="page-header">Change your Password</h2>
                {{ Form::open(array('role'=>'form','class'=>'form-horizontal')) }}
                <div class="form-group {{ $errors->first('old_password', ' has-error') }}">
                    <div class="col-xs-4">
                        {{Form::label('old_password', 'Old Password')}}
                        {{Form::password('old_password', array('class'=>'form-control', 'placeholder'=>'Old Password','autofocus'=>'autofocus'))}}
                        {{$errors->first('old_password', '<span class="help-block">:message</span>')}}
                    </div>
                </div>
                <div class="form-group {{ $errors->first('password', ' has-error') }}">
                    <div class="col-xs-4">
                        {{Form::label('password', 'New Password')}}
                        {{Form::password('password', array('class'=>'form-control', 'placeholder'=>'New Password'))}}
                        {{$errors->first('password', '<span class="help-block">:message</span>')}}
                    </div>
                </div>
                <div class="form-group {{ $errors->first('password_confirm', ' has-error') }}">
                    <div class="col-xs-4">
                        {{Form::label('password_confirm', 'Confirm New Password')}}
                        {{Form::password('password_confirm', array('class'=>'form-control', 'placeholder'=>'Confirm New Password'))}}
                        {{$errors->first('password_confirm', '<span class="help-block">:message</span>')}}
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <a class="btn" href="{{ route('admin') }}">Cancel</a>
                        {{ Form::submit('Update Password', array('class'=>'btn btn-large btn-success'))}}
<!--                        <a href="{{ route('forgot-password') }}" class="btn btn-link">I forgot my password</a>-->
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop