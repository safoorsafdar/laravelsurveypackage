@extends('survey::backend/layouts/dashboard')
@section('title')
Change your Email
@parent
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 panel-body">
        <ul class="nav nav-tabs nav-justified">
            <li><a href="{{ route('profile') }}">Your Profile</a></li>
            <li><a href="{{route('change-password')}}">Change Password</a></li>
            <li class="active"><a href="{{route('change-email')}}">Change Email</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <h2 class="page-header">Change your Email</h2>
                {{ Form::open(array('role'=>'form','class'=>'form-horizontal')) }}
                <div class="form-group {{ $errors->first('email', ' has-error') }}">
                    <div class="col-xs-4">
                        {{Form::label('email', 'New Email')}}
                        {{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'New Email','autofocus'=>'autofocus')) }}
                        {{$errors->first('email', '<span class="help-block">:message</span>')}}
                    </div>
                </div>
                <div class="form-group {{ $errors->first('email_confirm', ' has-error') }}">
                    <div class="col-xs-4">
                        {{Form::label('email_confirm', 'Confirm New Email')}}
                        {{ Form::text('email_confirm', null, array('class'=>'form-control', 'placeholder'=>'Confirm New Email','autofocus'=>'autofocus')) }}
                        {{$errors->first('email_confirm', '<span class="help-block">:message</span>')}}
                    </div>
                </div>
                <div class="form-group {{ $errors->first('current_password', ' has-error') }}">
                    <div class="col-xs-4">
                        {{Form::label('current_password', 'Current Password')}}
                        {{Form::password('current_password', array('class'=>'form-control', 'placeholder'=>'Current Password'))}}
                        {{$errors->first('current_password', '<span class="help-block">:message</span>')}}
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <a class="btn" href="{{ route('admin') }}">Cancel</a>
                        {{ Form::submit('Update Email', array('class'=>'btn btn-large btn-success'))}}
<!--                        <a href="{{ route('forgot-password') }}" class="btn btn-link">I forgot my password</a>-->
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop