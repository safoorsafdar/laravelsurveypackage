@extends('survey::backend/layouts/dashboard')
@section('title')
Employee Update ::
@parent
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h3>
                Employee Update
                <div class="pull-right">
                    <a href="{{ route('employees') }}" class="btn-sm btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
            </h3>
        </div>
        {{ Form::open(array('class'=>'form-horizontal','role'=>'form')) }}
        <fieldset>
            <legend>General Information</legend>
            <div class="form-group">
                {{Form::label('first_name', 'First Name',array("class"=>"control-label col-sm-2"))}}
                <div class="col-sm-5">
                    {{ Form::text('first_name', Input::old('first_name', $user->first_name), array('class'=>'form-control', 'placeholder'=>'First Name','autofocus'=>'autofocus')) }}
                    {{$errors->first('first_name', '<span class="help-block">:message</span>')}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('last_name', 'Last Name',array("class"=>"control-label col-sm-2"))}}
                <div class="col-sm-5">
                    {{ Form::text('last_name', Input::old('last_name', $user->last_name), array('class'=>'form-control', 'placeholder'=>'Last Name')) }}
                    {{$errors->first('last_name', '<span class="help-block">:message</span>')}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('email', 'Email',array("class"=>"control-label col-sm-2"))}}
                <div class="col-sm-5">
                    {{ Form::text('email', Input::old('email', $user->email), array('class'=>'form-control', 'placeholder'=>'Email')) }}
                    {{$errors->first('email', '<span class="help-block">:message</span>')}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('password', 'Password',array("class"=>"control-label col-sm-2"))}}
                <div class="col-sm-5">
                    {{ Form::password('password',  array('class'=>'form-control', 'placeholder'=>'Password')) }}
                    {{$errors->first('password', '<span class="help-block">:message</span>')}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('password_confirm', 'Confirm Password',array("class"=>"control-label col-sm-2"))}}
                <div class="col-sm-5">
                    {{ Form::password('password_confirm',  array('class'=>'form-control', 'placeholder'=>'Confirm Password')) }}
                    {{$errors->first('password_confirm', '<span class="help-block">:message</span>')}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('activated', 'Emp. Activated',array("class"=>"control-label col-sm-2"))}}
                <div class="col-sm-5">
                    <select {{ ($user->id === Sentry::getUser()->id ? ' disabled="disabled"' : '') }} name="activated" id="activated">
                        <option value="1"{{ ($user->isActivated() ? ' selected="selected"' : '') }}>@lang('survey::general.yes')</option>
                        <option value="0"{{ ( ! $user->isActivated() ? ' selected="selected"' : '') }}>@lang('survey::general.no')</option>
                    </select>
                    {{$errors->first('activated', '<span class="help-block">:message</span>')}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('groups', 'Groups',array("class"=>"control-label col-sm-2"))}}
                <div class="col-sm-5">
                    <select name="groups[]" id="groups[]" multiple="multiple">
                        @foreach ($groups as $group)
                        <option value="{{ $group->id }}"{{ (array_key_exists($group->id, $userGroups) ? ' selected="selected"' : '') }}>{{ $group->name }}</option>
                        @endforeach
                    </select>
                    {{$errors->first('groups', '<span class="help-block">:message</span>')}}
                    <span id="helpBlock" class="help-block">Select a group to assign to the user, remember that a user takes on the permissions of the group they are assigned.</span>
                </div>
            </div>
        </fieldset>
        <!-- Form Actions -->
        <hr>
        <div class="form-group">
            <div class="col-md-12">
                <a class="btn btn-link" href="{{ route('employees') }}">Cancel</a>
                <button type="reset" class="btn">Reset</button>
                {{ Form::submit('Update Employee', array('class'=>'btn btn-success'))}}
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop