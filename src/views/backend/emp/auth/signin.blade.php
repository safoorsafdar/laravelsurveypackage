@extends('survey::backend/layouts/default')

{{-- Page title --}}
@section('title')
Account Sign in ::
@parent
@stop
{{-- Page content --}}
@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Please Sign In</h3>
            </div>
            <div class="panel-body">
                {{ Form::open(array('route'=>'signin', 'class'=>'form-signin','role'=>'form',"data-parsley-validate"=>"data-parsley-validate")) }}
                    <fieldset>
                        <div class="form-group">
                            {{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Email Address','autofocus'=>'autofocus',"data-parsley-trigger"=>"change","required"=>"required","data-parsley-type"=>"email")) }}
                            {{ $errors->first('email', '<span class="help-block">:message</span>') }}
                        </div>
                        <div class="form-group">
                            <!--<input class="form-control" placeholder="Password" name="password" type="password" value="">-->
                            {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password',"data-parsley-trigger"=>"change","required"=>"required")) }}
                            {{ $errors->first('password', '<span class="help-block">:message</span>') }}
                        </div>
                        <div class="checkbox">
                            <label>
                                <!--<input name="remember" type="checkbox" value="Remember Me">Remember Me-->
                                {{Form::checkbox('remember-me', 'value');}} Remember Me
                            </label>
                        </div>
                        <!-- Change this to a button or input when using this as a form -->
                        <!--<a href="index.html" class="btn btn-lg btn-success btn-block">Login</a>-->
                        <div class="control-group">
                            <div class="controls">
                                    <!--<a class="btn" href="{{ route('admin') }}">Cancel</a>-->
                                    <!--<button type="submit" class="btn">Sign in</button>-->
                                    {{ Form::submit('Sign in', array('class'=>'btn btn-large btn-success btn-block'))}}
                                    <a href="{{ route('forgot-password') }}" class="btn btn-link">I forgot my password</a>
                            </div>
                        </div>
                    </fieldset>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop
