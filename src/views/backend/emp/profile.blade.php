@extends('survey::backend/layouts/dashboard')
@section('title')
Your Profile
@parent
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 panel-body">
        <ul class="nav nav-tabs nav-justified">
            <li class="active"><a href="{{ route('profile') }}">Your Profile</a></li>
            <li><a href="{{route('change-password')}}">Change Password</a></li>
            <li><a href="{{route('change-email')}}">Change Email</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <h2 class="page-header">Update your Profile</h2>
                {{ Form::open(array('role'=>'form','class'=>'form-horizontal')) }}
                <div class="form-group {{ $errors->first('first_name', ' has-error') }}">
                    <div class="col-xs-4">
                        {{Form::label('first_name', 'First Name')}}
                        {{ Form::text('first_name', $user->first_name, array('class'=>'form-control', 'placeholder'=>'First Name','autofocus'=>'autofocus')) }}
                        {{$errors->first('first_name', '<span class="help-block">:message</span>')}}
                    </div>
                </div>
                <div class="form-group {{ $errors->first('last_name', ' has-error') }}">
                    <div class="col-xs-4">
                        {{Form::label('last_name', 'Last Name')}}
                        {{ Form::text('last_name', $user->last_name, array('class'=>'form-control', 'placeholder'=>'Last Name')) }}
                        {{$errors->first('last_name', '<span class="help-block">:message</span>')}}
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <a class="btn" href="{{ route('admin') }}">Cancel</a>
                        {{ Form::submit('Update your Profile', array('class'=>'btn btn-large btn-success'))}}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@stop