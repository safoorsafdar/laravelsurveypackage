@extends('survey::backend/layouts/default')

{{-- Page title --}}
@section('title')
Forgot Password ::
@parent
@stop

{{-- Page content --}}
@section('content')
<!--<div class="page-header">
	<h3>Forgot Password</h3>
</div>
    <form method="post" action="" class="form-horizontal">
	 CSRF Token 
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
	 Email 
	<div class="control-group{{ $errors->first('email', ' error') }}">
		<label class="control-label" for="email">Email</label>
		<div class="controls">
			<input type="text" name="email" id="email" value="{{ Input::old('email') }}" />
			{{ $errors->first('email', '<span class="help-block">:message</span>') }}
		</div>
	</div>

	 Form actions 
	<div class="control-group">
		<div class="controls">
			<a class="btn" href="{{ route('signin') }}">Cancel</a>
			<button type="submit" class="btn">Submit</button>
		</div>
	</div>
</form>-->
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Forgot Password</h3>
            </div>
            <div class="panel-body">
                {{ Form::open(array('class'=>'form-signin','role'=>'form')) }}
                <fieldset>
                    <div class="form-group">
                        {{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Email Address','autofocus'=>'autofocus')) }}
                        {{ $errors->first('email', '<span class="help-block">:message</span>') }}
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <a class="btn btn-default" href="{{ route('signin') }}">Cancel</a>
                            {{ Form::submit('Sign in', array('class'=>'btn btn-large btn-success'))}}
                        </div>
                    </div>
                </fieldset>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop
