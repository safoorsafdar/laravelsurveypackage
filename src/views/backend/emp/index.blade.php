@extends('survey::backend/layouts/dashboard')
@section('title')
Employee Management ::
@parent
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h3>
                Employees Management
                <div class="pull-right">
                    <a href="{{route('create/emp')}}" class="btn btn-small btn-info"><i class="fa fa-plus"></i> Create</a>
                </div>
            </h3>
        </div>
        {{$users->links()}}

        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th class="span1">@lang('survey::admin/users/table.id')</th>
                    <th class="span2">@lang('survey::admin/users/table.first_name')</th>
                    <th class="span2">@lang('survey::admin/users/table.last_name')</th>
                    <th class="span3">@lang('survey::admin/users/table.email')</th>
                    <th class="span2">@lang('survey::admin/users/table.activated')</th>
                    <th class="span2">@lang('survey::admin/users/table.created_at')</th>
                    <th class="span2">@lang('survey::table.actions')</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->first_name }}</td>
                    <td>{{ $user->last_name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>@lang('survey::general.' . ($user->isActivated() ? 'yes' : 'no'))</td>
                    <td>{{ $user->created_at->diffForHumans() }}</td>
                    <td>
                        @if(Sentry::getUser()->hasAnyAccess(array("empolyee.edit")))
                        <a href="{{ route('update/emp', $user->id) }}" class="btn btn-sm btn-default">@lang('survey::button.edit')</a>
                        @endif
                        @if ( ! is_null($user->deleted_at))
                        <a href="{{ route('restore/emp', $user->id) }}" class="btn btn-sm btn-warning">@lang('survey::button.restore')</a>
                        @else
                        @if (Sentry::getUser()->id !== $user->id && Sentry::getUser()->hasAnyAccess(array("survey::empolyee.delete")))
                        <a href="{{ route('delete/emp', $user->id) }}" class="btn btn-sm btn-danger">@lang('survey::button.delete')</a>
                        @else
                        <span class="btn btn-sm btn-danger disabled">@lang('survey::button.delete')</span>
                        @endif
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        {{ $users->links() }}    
    </div>
</div>
@stop
