@extends('survey::backend/layouts/dashboard')
@section('title')
Groups Management ::
@parent
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h3>
                Group Management
                <div class="pull-right">
                    <a href="{{route('create/group')}}" class="btn btn-small btn-info"><i class="fa fa-plus"></i> Create</a>
                </div>
            </h3>
        </div>
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th class="span1">ID</th>
                    <th class="span2">Name</th>
                    <th class="span2">Permission</th>
                    <th class="span2">Created At</th>
                    <th class="span2">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($groups as $group)
                <tr>
                    <td>{{ $group->id }}</td>
                    <td>{{ $group->name }}</td>
                    <td>{{ json_encode($group->permissions) }}</td>
                    <td>{{ $group->created_at->diffForHumans() }}</td>
                    <td>
                        <a href="{{ route('update/group', $group->id) }}" class="btn btn-sm btn-default">@lang('survey::button.edit')</a>
                        @if ( ! is_null($group->deleted_at))
                        <a href="{{ route('restore/group', $group->id) }}" class="btn btn-sm btn-warning">@lang('survey::button.restore')</a>
                        @else
                        @if($group->id != 1)
                        <a href="{{ route('delete/group', $group->id) }}" class="btn btn-sm btn-danger">@lang('survey::button.delete')</a>
                        @endif
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
            </tbody>
        </table>

    </div>
</div>
@stop