@extends('survey::backend/layouts/dashboard')
@section('title')
Create a Group ::
@parent
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h3>
                Create a New Group
                <div class="pull-right">
                    <a href="{{ route('groups') }}" class="btn-sm btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
            </h3>
        </div>
        {{ Form::open(array('class'=>'form-horizontal','role'=>'form')) }}
        <fieldset>
            <legend>General Information</legend>
            <div class="form-group">
                {{Form::label('name', 'Name',array("class"=>"control-label col-sm-2"))}}
                <div class="col-md-3">
                    {{ Form::text('name', null, array('class'=>'form-control', 'placeholder'=>'Name','autofocus'=>'autofocus')) }}
                    {{$errors->first('name', '<span class="help-block">:message</span>')}}
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Permission</legend>
            <table class="table table-hover table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">&nbsp;</th>
                        <th class="text-center"><input type="checkbox" onchange="checkAll(this, '_view')" /><br/>View</th>
                        <th class="text-center"><input type="checkbox" onchange="checkAll(this, '_add')" /><br/>Add</th>
                        <th class="text-center"><input type="checkbox" onchange="checkAll(this, '_edit')" /><br/>Edit</th>
                        <th class="text-center"><input type="checkbox" onchange="checkAll(this, '_delete')" /><br/>Delete</th>
                        <th class="text-center"><input type="checkbox" onchange="checkAll(this, '_all')" /><br/>All</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($permissions as $area => $permissions)
                    <tr>
                        <th class="text-right">{{$area}}</th>
                        <th class="text-center"><input type="checkbox" value="1" id="{{ $permissions['permission'] }}_view" name="perm[{{ $permissions['permission'].".view" }}]" /></th>
                        <th class="text-center"><input type="checkbox" value="1" id="{{ $permissions['permission'] }}_add" name="perm[{{ $permissions['permission'].".add" }}]" /></th>
                        <th class="text-center"><input type="checkbox" value="1" id="{{ $permissions['permission'] }}_edit" name="perm[{{ $permissions['permission'].".edit" }}]" /></th>
                        <th class="text-center"><input type="checkbox" value="1" id="{{ $permissions['permission'] }}_delete" name="perm[{{ $permissions['permission'].".delete" }}]" /></th>
                        <th class="text-center"><input type="checkbox" value="1" id="{{ $permissions['permission'] }}_all" name="perm[{{ $permissions['permission'].".all" }}]" /></th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$errors->first('perm', '<span class="help-block">:message</span>')}}
        </fieldset>
        <!-- Form Actions -->
        <hr>
        <div class="form-group">
            <div class="col-md-12">
                <a class="btn btn-link" href="{{ route('groups') }}">Cancel</a>
                <button type="reset" class="btn">Reset</button>
                {{ Form::submit('Create Group', array('class'=>'btn btn-success'))}}
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop
