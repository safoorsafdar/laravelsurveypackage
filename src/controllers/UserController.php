<?php

namespace Vteam\Survey\Controllers\FrontEnd;

use Illuminate\Support\MessageBag;
//use Vteam\Survey\Controllers\AuthorizedController;
use View,
    Input,
    Redirect,
    Validator,
    Lang
,
    Sentry;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author safoor
 */
class UserController extends \BaseController {

    protected $messageBag;

    function __construct(MessageBag $messages) {
        $this->messageBag = $messages;
    }

    public function getIndex() {
        return View::make('survey::frontend/user/index');
    }

    public function getLogin() {
        // Is the user logged in?
        if (\Sentry::check()) {
            return Redirect::route('user/main');
        }
        return View::make('survey::frontend/user/login');
    }

    public function postLogin() {
        // Declare the rules for the form validation
        $rules = array(
            'email' => 'required|email',
            'password' => 'required|between:3,32',
        );

        // Create a new validator instance from our validation rules
        $validator = Validator::make(Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::back()->withInput()->withErrors($validator);
        }
        try {
            // Try to log the user in
            \Sentry::authenticate(Input::only('email', 'password'), Input::get('remember-me', 0));

            // Get the page we were before
            $redirect = \Session::get('loginRedirect', 'user');

            // Unset the page we were before from the session
            \Session::forget('loginRedirect');

            // Redirect to the users page
            return Redirect::to($redirect)->with('success', Lang::get('survey::auth/message.signin.success'));
        } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
            $this->messageBag->add('email', Lang::get('survey::auth/message.account_not_found'));
        } catch (\Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            $this->messageBag->add('email', Lang::get('survey::auth/message.account_not_activated'));
        } catch (\Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
            $this->messageBag->add('email', Lang::get('survey::auth/message.account_suspended'));
        } catch (\Cartalyst\Sentry\Throttling\UserBannedException $e) {
            $this->messageBag->add('email', Lang::get('survey::auth/message.account_banned'));
        }

        // Ooops.. something went wrong
        return Redirect::back()->withInput()->withErrors($this->messageBag);
        //return View::make('survey::frontend/user/login');
    }

    /**
     * Logout page.
     *
     * @return Redirect
     */
    public function getLogout() {
        // Log the user out
        \Sentry::logout();
        // Redirect to the users page
        return Redirect::route('home')->with('success', 'You have successfully logged out!');
    }

    public function getForgotPassword() {
        return View::make('survey::frontend/user/forgot-password');
    }

    public function postForgotPassword() {
        // Declare the rules for the validator
        $rules = array('email' => 'required|email');

        // Create a new validator instance from our dynamic rules
        $validator = Validator::make(Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::back()->withInput()->withErrors($validator);
        }

        try {
            // Get the user password recovery code
            $user = Sentry::getUserProvider()->findByLogin(Input::get('email'));

            // Data to be used on the email view
            $data = array(
                'user' => $user,
                'forgotPasswordUrl' => \URL::route('user/forgot-password-confirm', $user->getResetPasswordCode()),
            );

            // Send the activation code through email
            \Mail::send('survey::emails.forgot-password', $data, function($m) use ($user) {
                $m->to($user->email, $user->first_name . ' ' . $user->last_name);
                $m->subject('Account Password Recovery');
            });
        } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
            // Even though the email was not found, we will pretend
            // we have sent the password reset code through email,
            // this is a security measure against hackers.
        }

        //  Redirect to the forgot password
        return Redirect::back()->with('success', Lang::get('survey::auth/message.forgot-password.success'));
    }

    /**
     * Account sign up.
     *
     * @return View
     */
    public function getSignup() {
        // Is the user logged in?
        if (Sentry::check()) {
            return Redirect::route('user/main');
        }

        // Show the page
        return View::make('survey::frontend/user/signup');
    }

    /**
     * Account sign up form processing.
     *
     * @return Redirect
     */
    public function postSignup() {
        // Declare the rules for the form validation
        $rules = array(
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'email_confirm' => 'required|email|same:email',
            'password' => 'required|between:3,32',
            'password_confirm' => 'required|same:password',
        );

        // Create a new validator instance from our validation rules
        $validator = Validator::make(Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::back()->withInput()->withErrors($validator);
        }

        try {
            // Register the user
            $user = Sentry::register(array(
                        'first_name' => Input::get('first_name'),
                        'last_name' => Input::get('last_name'),
                        'email' => Input::get('email'),
                        'password' => Input::get('password'),
            ));

            // Data to be used on the email view
            $data = array(
                'user' => $user,
                'activationUrl' => \URL::route('user/activate', $user->getActivationCode()),
            );

            // Send the activation code through email
            \Mail::send('survey::emails.register-activate', $data, function($m) use ($user) {
                $m->to($user->email, $user->first_name . ' ' . $user->last_name);
                $m->subject('Welcome ' . $user->first_name);
            });

            // Redirect to the register page
            return Redirect::back()->with('success', Lang::get('survey::auth/message.signup.success'));
        } catch (\Cartalyst\Sentry\Users\UserExistsException $e) {
            $this->messageBag->add('email', Lang::get('survey::auth/message.account_already_exists'));
        }

        // Ooops.. something went wrong
        return Redirect::back()->withInput()->withErrors($this->messageBag);
    }

    /**
     * User account activation page.
     *
     * @param  string  $actvationCode
     * @return
     */
    public function getActivate($activationCode = null) {
        if (Sentry::check()) {
            return Redirect::route('user/main');
        }

        try {
            $user = Sentry::getUserProvider()->findByActivationCode($activationCode);


            if ($user->attemptActivation($activationCode)) {
                //updating user permission
                $permissions = (array("user" => "1"));
                $user->permissions = $permissions;
                $user->save();
                return Redirect::route('user/login')->with('success', Lang::get('survey::auth/message.activate.success'));
            }
            $error = Lang::get('survey::auth/message.activate.error');
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            $error = Lang::get('survey::auth/message.activate.error');
        }
        return Redirect::route('user/login')->with('error', $error);
    }

    /**
     * Forgot Password Confirmation page.
     *
     * @param  string  $passwordResetCode
     * @return View
     */
    public function getForgotPasswordConfirm($passwordResetCode = null) {
        try {
            // Find the user using the password reset code
            $user = Sentry::getUserProvider()->findByResetPasswordCode($passwordResetCode);
        } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
            // Redirect to the forgot password page
            return Redirect::route('user/forgot-password')->with('error', Lang::get('survey::auth/message.account_not_found'));
        }

        // Show the page
        return View::make('survey::frontend/user/forgot-password-confirm');
    }

    /**
     * Forgot Password Confirmation form processing page.
     *
     * @param  string  $passwordResetCode
     * @return Redirect
     */
    public function postForgotPasswordConfirm($passwordResetCode = null) {
        // Declare the rules for the form validation
        $rules = array(
            'password' => 'required',
            'password_confirm' => 'required|same:password'
        );

        // Create a new validator instance from our dynamic rules
        $validator = Validator::make(Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::route('user/forgot-password-confirm', $passwordResetCode)->withInput()->withErrors($validator);
        }

        try {
            // Find the user using the password reset code
            $user = Sentry::getUserProvider()->findByResetPasswordCode($passwordResetCode);

            // Attempt to reset the user password
            if ($user->attemptResetPassword($passwordResetCode, Input::get('password'))) {
                // Password successfully reseted
                return Redirect::route('user/login')->with('success', Lang::get('survey::auth/message.forgot-password-confirm.success'));
            } else {
                // Ooops.. something went wrong
                return Redirect::route('user/login')->with('error', Lang::get('survey::auth/message.forgot-password-confirm.error'));
            }
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            // Redirect to the forgot password page
            return Redirect::route('user/forgot-password')->with('error', Lang::get('survey::auth/message.account_not_found'));
        }
    }

}
