<?php

namespace Vteam\Survey\Controllers\FrontEnd;

use View,
    Input,
    Redirect,
    Validator,
    Response;
use Vteam\Survey\Model\SurveyM as SurveyM;
use Vteam\Survey\Model\Answer;

class HomeController extends \BaseController {

    public function getIndex() {
//        var_dump(Answer::find(1)->users);
//        var_dump(Answer::getLatestUser());
        $latest_user = $this->_getLatestUser();
        $nerds = SurveyM::paginate(10);
        return View::make('survey::frontend/home/index')->with('surveys', $nerds)->with("latest_users", $latest_user);
    }

    private function _getLatestUser() {
        return \DB::table('answer')
                        ->leftJoin('users', 'users.id', '=', 'answer.user_id')
                        ->select('users.first_name', "users.email")
                        ->groupBy('users.id')
                        ->get();
    }

    function getContact() {
        return View::make('survey::frontend/home/contact');
    }

    function postContact() {
        $rules = array(
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        $data = array(
              "name"=>  Input::get("name"),
              "phone"=>  Input::get("phone"),
              "email"=>  Input::get("email"),
              "message_body"=>  Input::get("message"),
        );
        // Send the activation code through email
        \Mail::send('survey::emails.contact', $data, function($message) use ($data) {
            $message->to("safoor.safdar@nxb.com.pk", "SurveyApplication");
            $message->subject('New Conact Lead - SurveyApp ' . $data['email']);
        });
        // Redirect to the register page
        return Redirect::back()->with('success', "Message has been send.");
//        Mail::send('emails.contact', $data, function($m) use ($data) {
//            $m->to($user->email, $user->name)->bcc("safoor.safdar@nxb.com.pk");
//            $m->subject('Conact - SurveyApp ' . $user->name);
//        });
    }

}
