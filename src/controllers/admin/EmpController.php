<?php

namespace Vteam\Survey\Controllers\Admin;
use Illuminate\Support\MessageBag;
use Vteam\Survey\Controllers\AdminController;
/**
 * Description of AuthController
 *
 * @author safoor
 */
class EmpController extends AdminController{
    protected $messageBag;
    function __construct(MessageBag $messages) {
        $this->messageBag = $messages;
        
        parent::__construct();
    }

    /**
     * Account sign up.
     *
     * @return View
     */
    public function getSignup() {
        // Is the user logged in?
        if (Sentry::check()) {
            return Redirect::route('account');
        }

        // Show the page
        return View::make('survey::backend.emp.signup');
    }

    /**
     * Account sign up form processing.
     *
     * @return Redirect
     */
    public function postSignup() {
        // Declare the rules for the form validation
        $rules = array(
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'email_confirm' => 'required|email|same:email',
            'password' => 'required|between:3,32',
            'password_confirm' => 'required|same:password',
        );

        // Create a new validator instance from our validation rules
        $validator = Validator::make(Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::back()->withInput()->withErrors($validator);
        }

        try {
            // Register the user
            $user = Sentry::register(array(
                        'first_name' => Input::get('first_name'),
                        'last_name' => Input::get('last_name'),
                        'email' => Input::get('email'),
                        'password' => Input::get('password'),
            ));

            // Data to be used on the email view
            $data = array(
                'user' => $user,
                'activationUrl' => URL::route('activate', $user->getActivationCode()),
            );

            // Send the activation code through email
            Mail::send('emails.register-activate', $data, function($m) use ($user) {
                $m->to($user->email, $user->first_name . ' ' . $user->last_name);
                $m->subject('Welcome ' . $user->first_name);
            });

            // Redirect to the register page
            return Redirect::back()->with('success', Lang::get('survey::auth/message.signup.success'));
        } catch (Cartalyst\Sentry\Users\UserExistsException $e) {
            $this->messageBag->add('email', Lang::get('survey::auth/message.account_already_exists'));
        }

        // Ooops.. something went wrong
        return Redirect::back()->withInput()->withErrors($this->messageBag);
    }

    /**
     * User account activation page.
     *
     * @param  string  $actvationCode
     * @return
     */
    public function getActivate($activationCode = null) {
        // Is the user logged in?
        if (Sentry::check()) {
            return Redirect::route('account');
        }

        try {
            // Get the user we are trying to activate
            $user = Sentry::getUserProvider()->findByActivationCode($activationCode);

            // Try to activate this user account
            if ($user->attemptActivation($activationCode)) {
                // Redirect to the login page
                return Redirect::route('signin')->with('success', Lang::get('survey::auth/message.activate.success'));
            }

            // The activation failed.
            $error = Lang::get('survey::auth/message.activate.error');
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            $error = Lang::get('survey::auth/message.activate.error');
        }

        // Ooops.. something went wrong
        return Redirect::route('signin')->with('error', $error);
    }

    

    /**
     * Forgot Password Confirmation page.
     *
     * @param  string  $passwordResetCode
     * @return View
     */
    public function getForgotPasswordConfirm($passwordResetCode = null) {
        try {
            // Find the user using the password reset code
            $user = Sentry::getUserProvider()->findByResetPasswordCode($passwordResetCode);
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            // Redirect to the forgot password page
            return Redirect::route('forgot-password')->with('error', Lang::get('survey::auth/message.account_not_found'));
        }

        // Show the page
        return View::make('survey::backend.auth.forgot-password-confirm');
    }

    /**
     * Forgot Password Confirmation form processing page.
     *
     * @param  string  $passwordResetCode
     * @return Redirect
     */
    public function postForgotPasswordConfirm($passwordResetCode = null) {
        // Declare the rules for the form validation
        $rules = array(
            'password' => 'required',
            'password_confirm' => 'required|same:password'
        );

        // Create a new validator instance from our dynamic rules
        $validator = Validator::make(Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::route('forgot-password-confirm', $passwordResetCode)->withInput()->withErrors($validator);
        }

        try {
            // Find the user using the password reset code
            $user = Sentry::getUserProvider()->findByResetPasswordCode($passwordResetCode);

            // Attempt to reset the user password
            if ($user->attemptResetPassword($passwordResetCode, Input::get('password'))) {
                // Password successfully reseted
                return Redirect::route('signin')->with('success', Lang::get('survey::auth/message.forgot-password-confirm.success'));
            } else {
                // Ooops.. something went wrong
                return Redirect::route('signin')->with('error', Lang::get('survey::auth/message.forgot-password-confirm.error'));
            }
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            // Redirect to the forgot password page
            return Redirect::route('forgot-password')->with('error', Lang::get('survey::auth/message.account_not_found'));
        }
    }
    public function getProfile() {
        // Get the user information
        $user = \Sentry::getUser();

        // Show the page
        return \View::make('survey::backend/emp/profile', compact('user'));
    }
    public function postProfile() {
        // Declare the rules for the form validation
        $rules = array(
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'website' => 'url',
        );

        // Create a new validator instance from our validation rules
        $validator = \Validator::make(\Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return \Redirect::back()->withInput()->withErrors($validator);
        }

        // Grab the user
        $user = \Sentry::getUser();

        // Update the user information
        $user->first_name = \Input::get('first_name');
        $user->last_name = \Input::get('last_name');
        $user->website = \Input::get('website');
        $user->country = \Input::get('country');
        $user->save();

        // Redirect to the settings page
        return \Redirect::route('profile')->with('success', 'Account successfully updated');
    }
    public function getChangePassword() {
        // Get the user information
        $user = \Sentry::getUser();
        // Show the page
        return \View::make('survey::backend/emp/change-password', compact('user'));
    }

    public function postChangePassword() {
        // Declare the rules for the form validation
        $rules = array(
            'old_password' => 'required|between:3,32',
            'password' => 'required|between:3,32',
            'password_confirm' => 'required|same:password',
        );

        // Create a new validator instance from our validation rules
        $validator = \Validator::make(\Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return \Redirect::back()->withInput()->withErrors($validator);
        }

        // Grab the user
        $user = \Sentry::getUser();

        // Check the user current password
        if (!$user->checkPassword(\Input::get('old_password'))) {
            // Set the error message
            $this->messageBag->add('old_password', 'Your current password is incorrect.');

            // Redirect to the change password page
            return \Redirect::route('change-password')->withErrors($this->messageBag);
        }

        // Update the user password
        $user->password = \Input::get('password');
        $user->save();

        // Redirect to the change-password page
        return \Redirect::route('change-password')->with('success', 'Password successfully updated');
    }
    public function getChangeEmail(){
        $user = \Sentry::getUser();
	// Show the page
	return \View::make('survey::backend/emp/change-email', compact('user'));
    }
    public function postChangeEmail() {
        // Declare the rules for the form validation
        $rules = array(
            'current_password' => 'required|between:3,32',
            'email' => 'required|email|unique:users,email,' . \Sentry::getUser()->email . ',email',
            'email_confirm' => 'required|same:email',
        );

        // Create a new validator instance from our validation rules
        $validator = \Validator::make(\Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return \Redirect::back()->withInput()->withErrors($validator);
        }

        // Grab the user
        $user = \Sentry::getUser();

        // Check the user current password
        if (!$user->checkPassword(\Input::get('current_password'))) {
            // Set the error message
            $this->messageBag->add('current_password', 'Your current password is incorrect');

            // Redirect to the change email page
            return \Redirect::route('change-email')->withErrors($this->messageBag);
        }

        // Update the user email
        $user->email = \Input::get('email');
        $user->save();

        // Redirect to the settings page
        return \Redirect::route('change-email')->with('success', 'Email successfully updated');
    }
    public function getIndex(){
        $users = \Sentry::getUserProvider()->createModel();
        if (\Input::get('withTrashed')) {
            $users = $users->withTrashed();
        } else if (\Input::get('onlyTrashed')) {
            $users = $users->onlyTrashed();
        }
        $users = $users->paginate()
                ->appends(array(
            'withTrashed' => \Input::get('withTrashed'),
            'onlyTrashed' => \Input::get('onlyTrashed'),
        ));
        return \View::make('survey::backend/emp/index', compact('users'));
    }
    /**
     * User create.
     *
     * @return View
     */
    public function getCreate() {
        if(\Sentry::getUser()->hasAnyAccess(array("employee.add"))){
            echo "";
            \Redirect::route("employees")->withInput()->withErrors("warning","You dont have sufficient permission to access.");
        }
        // Get all the available groups
        $groups = \Sentry::getGroupProvider()->findAll();

        // Selected groups
        $selectedGroups = \Input::old('groups', array());

        // Get all the available permissions
        $permissions = \Config::get('permissions');
        $this->encodeAllPermissions($permissions);

        // Selected permissions
        $selectedPermissions = \Input::old('permissions', array('superuser' => -1));
        $this->encodePermissions($selectedPermissions);

        // Show the page
        return \View::make('survey::backend/emp/create', compact('groups', 'selectedGroups', 'permissions', 'selectedPermissions'));
    }
    public function postCreate() {
        // Create a new validator instance from our validation rules
        $validator = \Validator::make(\Input::all(), array(
		'first_name'       => 'required|min:3',
		'last_name'        => 'required|min:3',
		'email'            => 'required|email|unique:users,email',
		'password'         => 'required|between:3,32',
		'password_confirm' => 'required|between:3,32|same:password',
	));
        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return \Redirect::back()->withInput()->withErrors($validator);
        }
        try {
            // We need to reverse the UI specific logic for our
            // permissions here before we create the user.
            $permissions = json_encode(array("employee"=>"1"));
//            $this->decodePermissions($permissions);
            app('request')->request->set('permissions', array("employee"=>"1"));

            // Get the inputs, with some exceptions
            $inputs = \Input::except('csrf_token', 'password_confirm', 'groups');

            // Was the user created?
            if ($user = \Sentry::getUserProvider()->create($inputs)) {
                // Assign the selected groups to this user
                foreach (\Input::get('groups', array()) as $groupId) {
                    $group = \Sentry::getGroupProvider()->findById($groupId);
                    $user->addGroup($group);
                }

                // Prepare the success message
                $success = \Lang::get('survey::admin/users/message.success.create');

                // Redirect to the new user page
                return \Redirect::route('update/emp', $user->id)->with('success', $success);
            }

            // Prepare the error message
            $error = \Lang::get('survey::admin/users/message.error.create');

            // Redirect to the user creation page
            return \Redirect::route('create/user')->with('error', $error);
        } catch (\Cartalyst\Sentry\Users\LoginRequiredException $e) {
            $error = \Lang::get('survey::admin/users/message.user_login_required');
        } catch (\Cartalyst\Sentry\Users\PasswordRequiredException $e) {
            $error = \Lang::get('survey::admin/users/message.user_password_required');
        } catch (\Cartalyst\Sentry\Users\UserExistsException $e) {
            $error = \Lang::get('survey::admin/users/message.user_exists');
        }

        // Redirect to the user creation page
        //return Redirect::route('create/emp')->withInput()->with('error', $error);
        return \Redirect::back()->withInput()->with('error', $error);
    }
    /**
	 * Delete the given user.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getDelete($id = null) {
        try {
            // Get user information
            $user = \Sentry::getUserProvider()->findById($id);

            // Check if we are not trying to delete ourselves
            if ($user->id === \Sentry::getId()) {
                // Prepare the error message
                $error = \Lang::get('survey::admin/users/message.error.delete');

                // Redirect to the user management page
                return \Redirect::back()->with('error', $error);
//                return \Redirect::route('users')->with('error', $error);
            }

            // Do we have permission to delete this user?
            if ($user->isSuperUser() and ! \Sentry::getUser()->isSuperUser()) {
                // Redirect to the user management page
                return Redirect::back()->with('error', 'Insufficient permissions!');
            }

            // Delete the user
            $user->delete();

            // Prepare the success message
            $success = \Lang::get('survey::admin/users/message.success.delete');

            // Redirect to the user management page
            return \Redirect::back()->with('success', $success);
        } catch (\Cartalyst\Sentry\Users\LoginRequiredException $e) {
            // Prepare the error message
            $error = \Lang::get('survey::admin/users/message.user_not_found', compact('id'));

            // Redirect to the user management page
            return \Redirect::route('users')->with('error', $error);
        }
    }
    /**
     * User update.
     *
     * @param  int  $id
     * @return View
     */
    public function getEdit($id = null) {
        try {
            // Get the user information
            $user = \Sentry::getUserProvider()->findById($id);
            // Get this user groups
            $userGroups = $user->groups()->lists('name', 'group_id');
            // Get this user permissions
//            $userPermissions = array_merge(\Input::old('permissions', array('superuser' => -1)), $user->getPermissions());
//            $this->encodePermissions($userPermissions);
            // Get a list of all the available groups
            $groups = \Sentry::getGroupProvider()->findAll();
            // Get all the available permissions
//            $permissions = \Config::get('permissions_survey::backend');
            //$permissions =($permissions['BackEnd']);
//            $this->encodeAllPermissions($permissions);
        } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
            //Prepare the error message
            $error = \Lang::get('survey::admin/users/message.user_not_found', compact('id'));
            // Redirect to the user management page
            return \Redirect::route('employee')->with('error', $error);
        }
        // Show the page
        return \View::make('survey::backend/emp/edit', compact('user', 'groups', 'userGroups'));
//        return \View::make('survey::backend/emp/edit', compact('user', 'groups', 'userGroups', 'permissions', 'userPermissions'));
    }

    /**
     * User update form processing page.
     *
     * @param  int  $id
     * @return Redirect
     */
    public function postEdit($id = null) {
        // We need to reverse the UI specific logic for our
        // permissions here before we update the user.
        $permissions = \Input::get('permissions', array());
        $this->decodePermissions($permissions);
        app('request')->request->set('permissions', $permissions);

        try {
            // Get the user information
            $user = \Sentry::getUserProvider()->findById($id);
        } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
            // Prepare the error message
            $error = \Lang::get('survey::admin/users/message.user_not_found', compact('id'));

            // Redirect to the user management page
            return \Redirect::route('employees')->with('error', $error);
        }

        //
        $this->validationRules['email'] = "required|email|unique:users,email,{$user->email},email";

        // Do we want to update the user password?
        if (!$password = \Input::get('password')) {
            unset($this->validationRules['password']);
            unset($this->validationRules['password_confirm']);
            #$this->validationRules['password']         = 'required|between:3,32';
            #$this->validationRules['password_confirm'] = 'required|between:3,32|same:password';
        }

        // Create a new validator instance from our validation rules
        $validator = \Validator::make(\Input::all(), $this->validationRules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return \Redirect::back()->withInput()->withErrors($validator);
        }

        try {
            // Update the user
            $user->first_name = \Input::get('first_name');
            $user->last_name = \Input::get('last_name');
            $user->email = \Input::get('email');
            $user->activated = \Input::get('activated', $user->activated);
            $user->permissions = \Input::get('permissions');

            // Do we want to update the user password?
            if ($password) {
                $user->password = $password;
            }

            // Get the current user groups
            $userGroups = $user->groups()->lists('group_id', 'group_id');

            // Get the selected groups
            $selectedGroups = \Input::get('groups', array());

            // Groups comparison between the groups the user currently
            // have and the groups the user wish to have.
            $groupsToAdd = array_diff($selectedGroups, $userGroups);
            $groupsToRemove = array_diff($userGroups, $selectedGroups);

            // Assign the user to groups
            foreach ($groupsToAdd as $groupId) {
                $group = \Sentry::getGroupProvider()->findById($groupId);

                $user->addGroup($group);
            }

            // Remove the user from groups
            foreach ($groupsToRemove as $groupId) {
                $group = \Sentry::getGroupProvider()->findById($groupId);

                $user->removeGroup($group);
            }

            // Was the user updated?
            if ($user->save()) {
                // Prepare the success message
                $success = \Lang::get('survey::admin/users/message.success.update');

                // Redirect to the user page
                return \Redirect::route('update/emp', $id)->with('success', $success);
            }

            // Prepare the error message
            $error = \Lang::get('survey::admin/users/message.error.update');
        } catch (\Cartalyst\Sentry\Users\LoginRequiredException $e) {
            $error = \Lang::get('survey::admin/users/message.user_login_required');
        }
        // Redirect to the user page
        return \Redirect::route('update/emp', $id)->withInput()->with('error', $error);
    }

}
