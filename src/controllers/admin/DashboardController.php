<?php
namespace Vteam\Survey\Controllers\Admin;
use Vteam\Survey\Controllers\AdminController;
class DashboardController extends AdminController {
    public function getIndex() {
        return \View::make('survey::backend/main');
    }
}
