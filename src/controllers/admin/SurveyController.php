<?php

namespace Vteam\Survey\Controllers\Admin;
use View,Input,Redirect,Validator,Survey,Lang;
use Vteam\Survey\Controllers\AdminController;
use Vteam\Survey\Model\SurveyM as SurveyM;
class SurveyController extends AdminController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $nerds = SurveyM::withTrashed()->paginate(2);
        return View::make('survey::backend/survey/index')->with('surveys', $nerds);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return View::make('survey::backend/survey/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $validator = Validator::make(Input::all(), SurveyM::$rules);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
            // store
            $nerd = new SurveyM;
            $nerd->name = Input::get('name');
            $nerd->detail = Input::get('detail');
            $nerd->save();
            // Prepare the success message
            $success = \Lang::get('survey::admin/users/message.success.create');
            // Redirect to the new user page
            return \Redirect::route('update/survey', $nerd->id)->with('success', $success);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $survey_detail = SurveyM::withTrashed()->find($id);
        return View::make('survey::backend/survey/edit')
            ->with('detail', $survey_detail);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
      $validator = Validator::make(Input::all(), SurveyM::$rules);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
            $nerd = SurveyM::withTrashed()->find($id);
            $nerd->name = Input::get('name');
            $nerd->detail = Input::get('detail');
            $nerd->save();
            // Prepare the success message
            // Redirect to the new user page
            return Redirect::route('update/survey', $nerd->id)->with('success', "Successfully updated the Survey!");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $nerd = SurveyM::find($id);
        $nerd->delete();
        return Redirect::back()->with('success', 'Successfully deleted the Survey!');
    }
    
    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function restore($id) {
        SurveyM::withTrashed()->where('id', $id)->restore();
        return Redirect::back()->with('success', 'Successfully restored the Survey!');
    }

}
