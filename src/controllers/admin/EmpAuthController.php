<?php

namespace Vteam\Survey\Controllers\Admin;

use Illuminate\Support\MessageBag;

/**
 * Description of EmpAuthController
 *
 * @author safoor
 */
class EmpAuthController extends \BaseController {

    function __construct(MessageBag $messages) {
        $this->messageBag = $messages;
    }

    public function getSignin() {
        if (\Sentry::check()) {
            return \Redirect::route('admin');
        }
        return \View::make('survey::backend/emp/auth/signin');
    }

    /**
     * Account sign in form processing.
     *
     * @return Redirect
     */
    public function postSignin() {
        // Declare the rules for the form validation
        $rules = array(
            'email' => 'required|email',
            'password' => 'required|between:3,32',
        );

        // Create a new validator instance from our validation rules
        $validator = \Validator::make(\Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return \Redirect::back()->withInput()->withErrors($validator);
        }
        try {
            // Try to log the user in
            \Sentry::authenticate(\Input::only('email', 'password'), \Input::get('remember-me', 0));

            // Get the page we were before
            $redirect = \Session::get('loginRedirect', 'admin');

            // Unset the page we were before from the session
            \Session::forget('loginRedirect');
            if (\Sentry::check()) {
                // Redirect to the users page
                return \Redirect::to($redirect)->with('success', \Lang::get('survey::auth/message.signin.success'));
            }else{
                return \Redirect::back()->withInput()->with("error","Something went wrong, please try later.");
            }
        } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
            $this->messageBag->add('email', \Lang::get('survey::auth/message.account_not_found'));
        } catch (\Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            $this->messageBag->add('email', \Lang::get('survey::auth/message.account_not_activated'));
        } catch (\Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
            $this->messageBag->add('email', \Lang::get('survey::auth/message.account_suspended'));
        } catch (\Cartalyst\Sentry\Throttling\UserBannedException $e) {
            $this->messageBag->add('email', \Lang::get('survey::auth/message.account_banned'));
        } catch (\Cartalyst\Sentry\Users\WrongPasswordException $e) {
            $this->messageBag->add('password', \Lang::get('survey::auth/message.wrong-pass'));
        }

        // Ooops.. something went wrong
        return \Redirect::back()->withInput()->withErrors($this->messageBag);
        //return \Redirect::back()->withInput()->with("error",$this->messageBag);
    }

    public function getLogout() {
        \Sentry::logout();
        return \Redirect::route('signin')->with('success', 'You have successfully logged out!');
    }

    /**
     * Forgot password page.
     *
     * @return View
     */
    public function getForgotPassword() {
        // Show the page
        return \View::make('survey::backend.emp.forgot-password');
    }

    /**
     * Forgot password form processing page.
     *
     * @return Redirect
     */
    public function postForgotPassword() {
        // Declare the rules for the validator
        $rules = array(
            'email' => 'required|email',
        );

        // Create a new validator instance from our dynamic rules
        $validator = \Validator::make(\Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return \Redirect::route('forgot-password')->withInput()->withErrors($validator);
        }

        try {
            // Get the user password recovery code
            $user = \Sentry::getUserProvider()->findByLogin(\Input::get('email'));

            // Data to be used on the email view
            $data = array(
                'user' => $user,
                'forgotPasswordUrl' => \URL::route('forgot-password-confirm', $user->getResetPasswordCode()),
            );

            // Send the activation code through email
            \Mail::send('emails.forgot-password', $data, function($m) use ($user) {
                $m->to($user->email, $user->first_name . ' ' . $user->last_name);
                $m->subject('Account Password Recovery');
            });
        } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
            // Even though the email was not found, we will pretend
            // we have sent the password reset code through email,
            // this is a security measure against hackers.
        }

        //  Redirect to the forgot password
        return \Redirect::route('forgot-password')->with('success', \Lang::get('survey::auth/message.forgot-password.success'));
    }

}
