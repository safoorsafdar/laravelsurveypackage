<?php

namespace Vteam\Survey\Controllers\Admin;

use Sentry;
use View;
use Config;
use Input;
use Illuminate\Support\MessageBag;
use Redirect;
use Vteam\Survey\Controllers\AdminController;
/**
 * Description of GroupController
 *
 * @author safoor
 */
class GroupController extends AdminController {

    protected $messageBag;

    function __construct(MessageBag $messages) {
        $this->messageBag = $messages;
    }

    public function getIndex() {
//        $groups = Sentry::getGroupProvider()->createModel(); // Get a list of all the available groups
        $groups = Sentry::findAllGroups();
//        $groups = Sentry::getGroupProvider()->findAll(); // Get a list of all the available groups
//        $groups->paginate();
//        $test = $groups->findAll();

        return \View::make('survey::backend/group/index', compact('groups'));
    }

    public function getCreate() {
        $permissions = Config::get('permissions_backend');
        return View::make('survey::backend/group/create', compact('permissions'));
    }

    public function postCreate() {
        // Create a new validator instance from our validation rules
        $validator = \Validator::make(\Input::all(), array(
                    'name' => 'required|min:3',
                    'perm' => 'required',
        ));
        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops..  something went wrong
            return \Redirect::back()->withInput()->withErrors($validator);
        }


        $permissions = Input::get('perm', array());
        $groupName = Input::get('name');

        try {
            $group = Sentry::createGroup(array(
                        'name' => $groupName,
                        'permissions' => $permissions
            ));
            return \Redirect::route('groups')->with('success', "Group Successfuly Created!");
        } catch (\Cartalyst\Sentry\Groups\NameRequiredException $e) {
            $this->messageBag->add('name', 'Name field is required');
        } catch (\Cartalyst\Sentry\Groups\GroupExistsException $e) {
            $this->messageBag->add('name', 'Group already exists.');
        }
        // Ooops.. something went wrong
        return Redirect::back()->withInput()->withErrors($this->messageBag);
    }
    /**
     * Group update.
     *
     * @param  int  $id
     * @return View
     */
    public function getEdit($id = null) {
        try {
            // Get the group information
            $group = Sentry::getGroupProvider()->findById($id);
            // Get all the available permissions
            $permissions = Config::get('permissions_backend');
            // Get this group permissions
            $groupPermissions = $group->getPermissions();
            $groupPermissions = array_merge($groupPermissions, Input::old('permissions', array()));
        } catch (\Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
            // Redirect to the groups management page
            return Redirect::route('groups')->with('error', Lang::get('survey::admin/groups/message.group_not_found', compact('id')));
        }

        // Show the page
        return View::make('survey::backend/group/edit', compact('group', 'permissions', 'groupPermissions'));
    }

    /**
     * Group update form processing page.
     *
     * @param  int  $id
     * @return Redirect
     */
    public function postEdit($id = null) {
        // We need to reverse the UI specific logic for our
        // permissions here before we update the group.
        $permissions = \Input::get('perm', array());
//        $this->decodePermissions($permissions);
        app('request')->request->set('permissions', $permissions);

        try {
            // Get the group information
            $group = \Sentry::getGroupProvider()->findById($id);
        } catch (\Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
            // Redirect to the groups management page
            return \Rediret::route('groups')->with('error', \Lang::get('survey::admin/groups/message.group_not_found', compact('id')));
        }

        // Declare the rules for the form validation
        $rules = array(
            'name' => 'required',
        );

        // Create a new validator instance from our validation rules
        $validator = \Validator::make(Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return \Redirect::back()->withInput()->withErrors($validator);
        }

        try {
//            print_r(Input::get('permissions'));
//            die();
            // Update the group data
            $group->name = \Input::get('name');
            $group->permissions = \Input::get('permissions');

            // Was the group updated?
            if ($group->save()) {
                // Redirect to the group page
                return \Redirect::route('update/group', $id)->with('success', \Lang::get('survey::admin/groups/message.success.update'));
            } else {
                // Redirect to the group page
                return \Redirect::route('update/group', $id)->with('error', \Lang::get('survey::admin/groups/message.error.update'));
            }
        } catch (\Cartalyst\Sentry\Groups\NameRequiredException $e) {
            $error = \Lang::get('admin/group/message.group_name_required');
        }

        // Redirect to the group page
        return \Redirect::route('update/group', $id)->withInput()->with('error', $error);
    }
    /**
	 * Delete the given group.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getDelete($id = null) {
        try {
            // Get group information
            $group = \Sentry::getGroupProvider()->findById($id);

            // Delete the group
            $group->delete();

            // Redirect to the group management page
            return \Redirect::route('groups')->with('success', \Lang::get('survey::admin/groups/message.success.delete'));
        } catch (\Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
            // Redirect to the group management page
            return \Redirect::route('groups')->with('error', Lang::get('survey::admin/groups/message.group_not_found', compact('id')));
        }
    }

}
