<?php

namespace Vteam\Survey\Controllers\Admin;

use View,
    Input,
    Redirect,
    Validator,
    Survey;
use Vteam\Survey\Controllers\AdminController;
use Vteam\Survey\Model\Question;
use Vteam\Survey\Model\SurveyM;

class QuestionController extends AdminController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $questions = Question::with('surveym')->withTrashed()->paginate(2);
        return View::make('survey::backend/question/index')->with('questions', $questions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return View::make('survey::backend/question/create')->with("surveyList", SurveyM::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $validator = Validator::make(Input::all(), Question::$rules);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
            $nerd = new Question;
            $nerd->name = Input::get('name');
            $nerd->type = Input::get('type');
            $nerd->exam_id = Input::get('surveyID');
            $nerd->save();
            return Redirect::route('update/question', $nerd->id)->with('success', "Question was successfully created. ");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $survey_detail = Question::withTrashed()->find($id);
        return View::make('survey::backend/question/edit')
                        ->with('detail', $survey_detail)
                        ->with("surveyList", SurveyM::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $validator = Validator::make(Input::all(), Question::$rules);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
            $nerd = Question::find($id);
            $nerd->name = Input::get('name');
            $nerd->type = Input::get('type');
            $nerd->exam_id = Input::get('surveyID');
            $nerd->save();
            return Redirect::route('update/question', $nerd->id)->with('success', "Question was successfully Updated. ");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $nerd = Question::find($id);
        $nerd->delete();
        return Redirect::back()->with('success', 'Successfully deleted the Question!');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function restore($id) {
        Question::withTrashed()->where('id', $id)->restore();
        return Redirect::back()->with('success', 'Successfully restored the Question!');
    }

}
