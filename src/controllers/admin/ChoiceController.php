<?php

namespace Vteam\Survey\Controllers\Admin;

use View,
    Input,
    Redirect,
    Validator;
use Vteam\Survey\Controllers\AdminController;
use Vteam\Survey\Model\Question;
use Vteam\Survey\Model\Choice;

class ChoiceController extends AdminController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $nerds = Choice::withTrashed()->paginate(2);
        return View::make('survey::backend/choice/index')->with('choices', $nerds);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return View::make('survey::backend/choice/create')->with("questionList", Question::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $validator = Validator::make(Input::all(), Choice::$rules);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
            $nerd = new Choice;
            $nerd->detail = Input::get('name');
            $nerd->question_id = Input::get('questionID');
            $nerd->isAnswer = Input::get('isAnswer');
            $nerd->save();
            return Redirect::route('update/choice', $nerd->id)->with('success', "Choice was successfully created. ");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $exam_detail = Choice::withTrashed()->find($id);
        return View::make('survey::backend/choice/edit')
                        ->with('detail', $exam_detail)
                        ->with("questionList", Question::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $validator = Validator::make(Input::all(), Choice::$rules);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
            $nerd = Choice::find($id);
            $nerd->detail = Input::get('name');
            $nerd->question_id = Input::get('questionID');
            $nerd->isAnswer = Input::get('isAnswer');
            $nerd->save();
            return Redirect::route('update/choice', $nerd->id)->with('success', "Choice was successfully Updated. ");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $nerd = Choice::find($id);
        $nerd->delete();
        return Redirect::back()->with('success', 'Successfully deleted the Choice!');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function restore($id) {
        Choice::withTrashed()->where('id', $id)->restore();
        return Redirect::back()->with('success', 'Successfully restored the Choice!');
    }

}
