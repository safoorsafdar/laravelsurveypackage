<?php

namespace Vteam\Survey\Controllers\FrontEnd;

use View,
    Input,
    Redirect,
    Validator,
    Response,
    Sentry;
use Vteam\Survey\Model\SurveyM as SurveyM;
use Vteam\Survey\Model\Question;
use Vteam\Survey\Model\Answer;

/**
 * Description of SurveyController
 *
 * @author safoor
 */
class SurveyController extends \BaseController {

    public function getShow($id) {
        $nerd = SurveyM::find($id);
        if ($nerd) {
            $nerd_question = Question::where('exam_id', '=', $id)->get();
//            foreach($nerd_question['list'] as $single){
//                $nerd_question['question'][] = Choice::where('question_id', '=', $single->id)->get();
//            }
            return View::make('survey::frontend/survey/show')->with('survey_detail', $nerd)->with("questions", $nerd_question);
        } else {
            return Response::view('survey::frontend/error/404', array(), 404);
        }
    }

    public function postShow($id) {
        // Is the user logged in?
        if (!Sentry::check()) {
            return Redirect::route('user/login')->with('warning', "First, You need to login to submit a survey.");
        }
        $rules = array('ans' => 'required');
        $messages = array('ans.required' => 'We need to know your atleast one choice!',);
        $validator = Validator::make(Input::all(), $rules, $messages);
        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $gesture_ans = Input::get("ans");
        //preparing data...
        foreach ($gesture_ans as $question => $choice) {
            
            foreach ($choice as $type => $val) {
                $ans = array();
                switch ($type) {
                    case 'radio':
                        $ans['answered'] = $val;
                        $ans["type"] = "radio";
                        break;
                    case 'box':
                        $ans['answered'] = implode(",", $val);
                        $ans["type"] = "box";
                        break;
                    case 'text':
                        $ans['answered'] = ($val);
                        $ans["type"] = "text";
                        break;
                }
            }
            //insert into db
            $answer = new Answer;
            $answer->exam_id = $id;
            $answer->question_id = $question;
            if (Sentry::check()) {
                $answer->user_id = Sentry::getUser()->id;
            }
            $answer->ans = serialize($ans);
            $answer->save();
        }
        return Redirect::route('home')->with('success', "Thank you so much");
    }

}
