<?php

Route::filter('admin-auth', function($request, $response) {
    if (!Sentry::check()) {
        Session::put('loginRedirect', Request::url());
        return Redirect::route('signin');
    }
//    if (Sentry::getUser()->hasAnyAccess(array('user'))) {
//        return Redirect::route('user/main')->with("warning", "You dont have sufficient Permission.");
//    }
    if (!Sentry::getUser()->hasAnyAccess(array('admin', 'employee'))) {
        Sentry::logout();
        return Redirect::route('signin')->with("warning", "You dont have sufficient Permission.");
    }
});

Route::filter('user-auth', function($request, $response) {
    if (!Sentry::check()) {
        Session::put('loginRedirect', Request::url());
        return Redirect::route('user/login');
    }
    if (!Sentry::getUser()->hasAnyAccess(array('user'))) {
        Sentry::logout();
        return Redirect::route('home')->with("warning", "You dont have sufficient Permission.");
    }
});
