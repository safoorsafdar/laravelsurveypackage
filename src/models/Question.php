<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
namespace Vteam\Survey\Model;
//use Illuminate\Database\Eloquent\Model as EloquentModel;
/**
 * Description of Question
 *
 * @author safoor
 */
class Question extends \Eloquent {
    use \SoftDeletingTrait;
    protected $table = "question";
    protected $dates = ['deleted_at']; //soft deleting
    public static $rules = array(
        'name' => 'required|min:3',
        'type' => 'required',
        'surveyID' => 'required',
    );
    public function surveym(){
        return $this->belongsTo('Vteam\Survey\Model\SurveyM');
    }
    public function choices(){
//        return $this->h
    }
}
