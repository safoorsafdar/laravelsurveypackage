<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
namespace Vteam\Survey\Model;
/**
 * Description of Exam
 *
 * @author safoor
 */
class SurveyM extends \Eloquent {
    use \SoftDeletingTrait;
    protected $table = "exam";
    protected $dates = ['deleted_at']; //soft deleting
    public static $rules = array(
        'name' => 'required|min:3',
    );
    
    public function question(){
        return $this->hasMany('Vteam\Survey\Model\Question','exam_id');
    }
    
}
