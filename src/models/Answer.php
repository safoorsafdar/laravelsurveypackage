<?php

namespace Vteam\Survey\Model;
/**
 * Description of Product
 *
 * @author safoor
 */
class Answer extends \Eloquent{
    //put your code here
    protected $table = "answer";
    public function users(){
        return $this->belongsTo('Vteam\Survey\Model\Users',"user_id");
    }
    
    
    public function scopeGetLatestUser($query){
//        return \DB::table('answer')
//        ->leftJoin('users', 'users.id', '=', 'answer.user_id')
//        ->select('users.first_name',"users.email")
//        ->groupBy('users.id')
//        ->get();
        return $query->leftJoin('users', 'users.id', '=', 'answer.user_id')->select('users.first_name',"users.email")->groupBy('users.id')->get();
    }
}
