<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
namespace Vteam\Survey\Model;
/**
 * Description of Choice
 *
 * @author safoor
 */
class Choice extends \Eloquent{
    use \SoftDeletingTrait;
    protected $table = "choice";
    protected $dates = ['deleted_at']; //soft deleting
    public static $rules = array(
        'name' => 'required|min:3',
        'questionID' => 'required',
        'isAnswer' => 'required',
    );
}
