<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChoiceTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('choice', function(Blueprint $table) {
            $table->increments('id');
            $table->text("detail", 100);
            $table->enum('isAnswer', array('yes', 'no'))->default("no");
            $table->softDeletes();
            $table->timestamps();
            //defining exam foreign key
            $table->integer('question_id')->unsigned();
            $table->foreign('question_id')->references('id')->on('question');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists("choice");
    }

}
