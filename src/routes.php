<?php

Route::get('/', array("as"=>"home", "uses"=>'Vteam\Survey\Controllers\FrontEnd\HomeController@getIndex'));
Route::get('contact', array("as"=>"contact", "uses"=>'Vteam\Survey\Controllers\FrontEnd\HomeController@getContact'));
Route::post('contact', 'Vteam\Survey\Controllers\FrontEnd\HomeController@postContact');
Route::get('/about', array("as"=>"about", "uses"=>'Vteam\Survey\Controllers\FrontEnd\HomeController@getAbout'));
Route::get('survey/{id}/show', array('as' => 'survey/detail', 'uses' => 'Vteam\Survey\Controllers\FrontEnd\SurveyController@getShow'))->where('id', '[0-9]+');
Route::post('survey/{id}/show', 'Vteam\Survey\Controllers\FrontEnd\SurveyController@postShow')->where('id', '[0-9]+');

Route::group(array('prefix' => 'user'), function() {
    #dashboard
    Route::get('/', array('as' => 'user/main', 'uses' => 'Vteam\Survey\Controllers\FrontEnd\UserController@getIndex','before' => 'user-auth'));
    # Login
    Route::get('login', array('as' => 'user/login', 'uses' => 'Vteam\Survey\Controllers\FrontEnd\UserController@getLogin'));
    Route::post('login', 'Vteam\Survey\Controllers\FrontEnd\UserController@postLogin');
    # signup
    Route::get('signup', array('as' => 'user/signup', 'uses' => 'Vteam\Survey\Controllers\FrontEnd\UserController@getSignup'));
    Route::post('signup', 'Vteam\Survey\Controllers\FrontEnd\UserController@postSignup');
    # Account Activation
    Route::get('activate/{activationCode}', array('as' => 'user/activate', 'uses' => 'Vteam\Survey\Controllers\FrontEnd\UserController@getActivate'));

    # Forgot Password
    Route::get('forgot-password', array('as' => 'user/forgot-password', 'uses' => 'Vteam\Survey\Controllers\FrontEnd\UserController@getForgotPassword'));
    Route::post('forgot-password', 'Vteam\Survey\Controllers\FrontEnd\UserController@postForgotPassword');

    # Forgot Password Confirmation
    Route::get('forgot-password/{passwordResetCode}', array('as' => 'user/forgot-password-confirm', 'uses' => 'Vteam\Survey\Controllers\FrontEnd\UserController@getForgotPasswordConfirm'));
    Route::post('forgot-password/{passwordResetCode}', 'Vteam\Survey\Controllers\FrontEnd\UserController@postForgotPasswordConfirm');
    # Logout
    Route::get('logout', array('as' => 'user/logout', 'before' => 'user-auth', 'uses' => 'Vteam\Survey\Controllers\FrontEnd\UserController@getLogout'));
});
/*
  |--------------------------------------------------------------------------
  | Admin Routes
  |--------------------------------------------------------------------------
  |
  | Register all the admin routes.
  |
 */
#Admin Authentication
Route::group(array('prefix' => 'adminAuth'), function() {
    # Login
    Route::get('signin', array('as' => 'signin', 'uses' => 'Vteam\Survey\Controllers\Admin\EmpAuthController@getSignin'));
    Route::post('signin', 'Vteam\Survey\Controllers\Admin\EmpAuthController@postSignin');
    # Logout
    Route::get('logout', array('as' => 'logout', 'before' => 'admin-auth', 'uses' => 'Vteam\Survey\Controllers\Admin\EmpAuthController@getLogout'));
    # Forgot Password
    Route::get('forgot-password', array('as' => 'forgot-password', 'uses' => 'Vteam\Survey\Controllers\Admin\EmpAuthController@getForgotPassword'));
    Route::post('forgot-password', 'Vteam\Survey\Controllers\Admin\EmpAuthController@postForgotPassword');

    # Forgot Password Confirmation
    Route::get('forgot-password/{passwordResetCode}', array('as' => 'forgot-password-confirm', 'uses' => 'Vteam\Survey\Controllers\Admin\EmpController@getForgotPasswordConfirm'));
    Route::post('forgot-password/{passwordResetCode}', 'Vteam\Survey\Controllers\Admin\EmpController@postForgotPasswordConfirm');
});

Route::group(array('prefix' => 'admin', 'before' => 'admin-auth'), function() {
    # Dashboard
    Route::get('/', array('as' => 'admin', 'uses' => 'Vteam\Survey\Controllers\Admin\DashboardController@getIndex'));
    /*
      |--------------------------------------------------------------------------
      | Authentication and Authorization Routes
      |--------------------------------------------------------------------------
      |
     */
    //Route::controller('auth', 'Vteam\Survey\Controllers\Admin\AuthController');
    Route::group(array('prefix' => 'emp'), function() {
        #Employees
        Route::get('/', array('as' => 'employees', 'uses' => 'Vteam\Survey\Controllers\Admin\EmpController@getIndex'));
        Route::get('create', array('as' => 'create/emp', 'uses' => 'Vteam\Survey\Controllers\Admin\EmpController@getCreate'));
        Route::post('create', 'Vteam\Survey\Controllers\Admin\EmpController@postCreate');
        Route::get('{empId}/edit', array('as' => 'update/emp', 'uses' => 'Vteam\Survey\Controllers\Admin\EmpController@getEdit'));
        Route::post('{empId}/edit', 'Vteam\Survey\Controllers\Admin\EmpController@postEdit');
        Route::get('{empId}/delete', array('as' => 'delete/emp', 'uses' => 'Vteam\Survey\Controllers\Admin\EmpController@getDelete'));
        Route::get('{empId}/restore', array('as' => 'restore/emp', 'uses' => 'Vteam\Survey\Controllers\Admin\EmpController@getRestore'));



        #Route::get('all', array('as' => 'empAll', 'uses' => 'Vteam\Survey\Controllers\Admin\EmpController@getAll'));
        # Register
//        Route::get('signup', array('as' => 'signup', 'uses' => 'Vteam\Survey\Controllers\Admin\EmpController@getSignup'));
//        Route::post('signup', 'EmpController@postSignup');

        # Account Activation
//        Route::get('activate/{activationCode}', array('as' => 'activate', 'uses' => 'Vteam\Survey\Controllers\Admin\EmpController@getActivate'));

        # Profile
        Route::get('profile', array('as' => 'profile', 'uses' => 'Vteam\Survey\Controllers\Admin\EmpController@getProfile'));
        Route::post('profile', 'Vteam\Survey\Controllers\Admin\EmpController@postProfile');
        # Change Password
        Route::get('change-password', array('as' => 'change-password', 'uses' => 'Vteam\Survey\Controllers\Admin\EmpController@getChangePassword'));
        Route::post('change-password', 'Vteam\Survey\Controllers\Admin\EmpController@postChangePassword');

        # Change Email
        Route::get('change-email', array('as' => 'change-email', 'uses' => 'Vteam\Survey\Controllers\Admin\EmpController@getChangeEmail'));
        Route::post('change-email', 'Vteam\Survey\Controllers\Admin\EmpController@postChangeEmail');
    });
    #Group
    Route::group(array('prefix' => 'group'), function() {
        Route::get('/', array('as' => 'groups', 'uses' => 'Vteam\Survey\Controllers\Admin\GroupController@getIndex'));
        Route::get('create', array('as' => 'create/group', 'uses' => 'Vteam\Survey\Controllers\Admin\GroupController@getCreate'));
        Route::post('create', 'Vteam\Survey\Controllers\Admin\GroupController@postCreate');
        Route::get('{groupId}/edit', array('as' => 'update/group', 'uses' => 'Vteam\Survey\Controllers\Admin\GroupController@getEdit'));
        Route::post('{groupId}/edit', 'Vteam\Survey\Controllers\Admin\GroupController@postEdit');
        Route::get('{groupId}/delete', array('as' => 'delete/group', 'uses' => 'Vteam\Survey\Controllers\Admin\GroupController@getDelete'));
        Route::get('{groupId}/restore', array('as' => 'restore/group', 'uses' => 'Vteam\Survey\Controllers\Admin\GroupController@getRestore'));
    });

    #Exam
//    Route::resource('exam', ('Vteam\Survey\Controllers\Admin\ExamController'), [
//        'names' => [
//            'index' => 'exam',
//            'create' => 'create/exam',
//            'destroy' => 'delete/exam',
//            'edit' => 'delete/exam',
//        ]
//    ]);
//    Route::resource('exam', ('Vteam\Survey\Controllers\Admin\ExamController'),array('as' => 'exam'));
//    Route::resource('exam', array('as' => 'exam', 'uses' => 'Vteam\Survey\Controllers\Admin\ExamController'));
//    Route::resource('exam','Vteam\Survey\Controllers\Admin\ExamController');
    #Group
    Route::group(array('prefix' => 'survey'), function() {
        Route::get('/', array('as' => 'survey', 'uses' => 'Vteam\Survey\Controllers\Admin\SurveyController@index'));
        Route::get('create', array('as' => 'create/survey', 'uses' => 'Vteam\Survey\Controllers\Admin\SurveyController@create'));
        Route::post('create', 'Vteam\Survey\Controllers\Admin\SurveyController@store');
        Route::get('{examId}/edit', array('as' => 'update/survey', 'uses' => 'Vteam\Survey\Controllers\Admin\SurveyController@edit'));
        Route::post('{examId}/edit', 'Vteam\Survey\Controllers\Admin\SurveyController@update');
        Route::get('{examId}/delete', array('as' => 'delete/survey', 'uses' => 'Vteam\Survey\Controllers\Admin\SurveyController@destroy'));
        Route::get('{examId}/restore', array('as' => 'restore/survey', 'uses' => 'Vteam\Survey\Controllers\Admin\SurveyController@restore'));
    });
    #Question
//    Route::resource('question', 'Vteam\Survey\Controllers\Admin\QuestionController');
    Route::group(array('prefix' => 'question'), function() {
        Route::get('/', array('as' => 'question', 'uses' => 'Vteam\Survey\Controllers\Admin\QuestionController@index'));
        Route::get('create', array('as' => 'create/question', 'uses' => 'Vteam\Survey\Controllers\Admin\QuestionController@create'));
        Route::post('create', 'Vteam\Survey\Controllers\Admin\QuestionController@store');
        Route::get('{examId}/edit', array('as' => 'update/question', 'uses' => 'Vteam\Survey\Controllers\Admin\QuestionController@edit'));
        Route::post('{examId}/edit', 'Vteam\Survey\Controllers\Admin\QuestionController@update');
        Route::get('{examId}/delete', array('as' => 'delete/question', 'uses' => 'Vteam\Survey\Controllers\Admin\QuestionController@destroy'));
        Route::get('{examId}/restore', array('as' => 'restore/question', 'uses' => 'Vteam\Survey\Controllers\Admin\QuestionController@restore'));
    });
    #Choice
//    Route::resource('choice', 'Vteam\Survey\Controllers\Admin\ChoiceController');
    Route::group(array('prefix' => 'choice'), function() {
        Route::get('/', array('as' => 'choice', 'uses' => 'Vteam\Survey\Controllers\Admin\ChoiceController@index'));
        Route::get('create', array('as' => 'create/choice', 'uses' => 'Vteam\Survey\Controllers\Admin\ChoiceController@create'));
        Route::post('create', 'Vteam\Survey\Controllers\Admin\ChoiceController@store');
        Route::get('{examId}/edit', array('as' => 'update/choice', 'uses' => 'Vteam\Survey\Controllers\Admin\ChoiceController@edit'));
        Route::post('{examId}/edit', 'Vteam\Survey\Controllers\Admin\ChoiceController@update');
        Route::get('{examId}/delete', array('as' => 'delete/choice', 'uses' => 'Vteam\Survey\Controllers\Admin\ChoiceController@destroy'));
        Route::get('{examId}/restore', array('as' => 'restore/choice', 'uses' => 'Vteam\Survey\Controllers\Admin\ChoiceController@restore'));
    });

    #Answer
    Route::resource('answer', 'Vteam\Survey\Controllers\Admin\AnswerController');
});
